from helpers.data import multi_line_data, run

empty_space, mirrors, splitters = '.', {'slash': '/', 'backslash': '\\'}, {'pipe': '|', 'dash': '-'}


def part1():
    data = multi_line_data()
    return get_energized_tiles_from_start((0, 0, (1, 0)), data)


def part2():
    data = multi_line_data()

    most_energized = 0
    for x, _ in enumerate(data[0]):
        energized = get_energized_tiles_from_start((x, 0, (0, 1)), data)
        most_energized = max(most_energized, energized)
    for x, _ in enumerate(data[-1]):
        energized = get_energized_tiles_from_start((x, 0, (0, -1)), data)
        most_energized = max(most_energized, energized)
    for y, _ in enumerate(data):
        energized = get_energized_tiles_from_start((0, y, (1, 0)), data)
        most_energized = max(most_energized, energized)
    for y, row in enumerate(data):
        energized = get_energized_tiles_from_start((len(row) - 1, y, (-1, 0)), data)
        most_energized = max(most_energized, energized)

    return most_energized


def get_new_direction_from_mirror(mirror: str, direction: tuple[int, int]):
    dirs = [(0, 1), (-1, 0), (0, -1), (1, 0)]
    if mirror == mirrors['slash']:
        return {dirs[0]: dirs[1], dirs[1]: dirs[0], dirs[2]: dirs[3], dirs[3]: dirs[2]}[direction]
    return {dirs[0]: dirs[3], dirs[1]: dirs[2], dirs[2]: dirs[1], dirs[3]: dirs[0]}[direction]


def get_split_laser_beams(splitter: str, x: int, y: int):
    if splitter == splitters['dash']:
        return [(x + 1, y, (1, 0)), (x - 1, y, (-1, 0))]
    return [(x, y + 1, (0, 1)), (x, y - 1, (0, -1))]


def get_energized_tiles_from_start(start: tuple[int, int, tuple], data: list[str]) -> int:
    active_lasers = [start]
    splits = set()
    energized = set()
    while len(active_lasers) != 0:
        for laser, (laser_x, laser_y, laser_direction) in enumerate(active_lasers):
            not_in_grid = laser_x < 0 or laser_x >= len(data[0]) or laser_y < 0 or laser_y >= len(data)
            if not_in_grid:
                active_lasers.remove((laser_x, laser_y, laser_direction))
                break

            spot = data[laser_y][laser_x]
            if (laser_x, laser_y) in splits:
                active_lasers.remove((laser_x, laser_y, laser_direction))
                break

            if spot in mirrors.values():
                new_direction = get_new_direction_from_mirror(spot, laser_direction)
                active_lasers[laser] = (laser_x, laser_y, new_direction)
                laser_direction = new_direction

            if spot in splitters.values():
                if ((spot == splitters['dash'] and laser_direction[0] == 0) or
                        (spot == splitters['pipe'] and laser_direction[1] == 0)):
                    active_lasers.extend(get_split_laser_beams(spot, laser_x, laser_y))
                    active_lasers.remove((laser_x, laser_y, laser_direction))
                    splits.add((laser_x, laser_y))
                else:
                    active_lasers[laser] = (laser_x + laser_direction[0], laser_y + laser_direction[1], laser_direction)
                energized.add((laser_x, laser_y))
                break

            energized.add((laser_x, laser_y))
            active_lasers[laser] = (laser_x + laser_direction[0], laser_y + laser_direction[1], laser_direction)

    return len(energized)


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
