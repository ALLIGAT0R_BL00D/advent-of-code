# Day 16: The Floor Will Be Lava

## [Part 1](https://adventofcode.com/2023/day/16)

I start my laser in the `top left`. A laser in represented by the following
tuple: `(x, y, (x direction, y direction))`. For every tile I go through
these steps:

1. Is the laser still in the grid? no, remove laser and stop
2. Did I use this `splitter` before? yes, remove laser and stop
3. Am I looking at a `mirror`?
    * Give me the new direction of the laser
    * Update the laser with the new direction
4. Am I looking at a `splitter`?
    * Is the splitter in the direction I'm going?
        * yes, just continue
        * No! Create `two new lasers` in the right directions and remove
          the current laser. Mark this `splitter` as used
    * Add tile to energized set
    * stop doing anything with this laser
5. Add tile to energized set
6. Update position of the laser

## [Part 2](https://adventofcode.com/2023/day/16#part2)

The same thing as Part 1, but now I start at every edge and keep track of
which laser energizes the most tiles
