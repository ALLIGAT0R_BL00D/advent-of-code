from helpers.data import run


def part1():
    # data = single_line_data('practice')
    # data = multi_line_data('practice.txt')

    return -1


def part2():
    # data = single_line_data('practice')
    # data = multi_line_data('practice.txt')

    return -1


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
