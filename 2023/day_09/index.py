from helpers.data import find_all_numbers, multi_line_data, run


def part1():
    data = multi_line_data()

    sequences = [find_all_numbers(sequence) for sequence in data]

    total = 0
    for sequence in sequences:
        extrapolated = get_extrapolated_data(sequence)
        extrapolated = predict(extrapolated)

        total += extrapolated[0][-1]
    return total


def part2():
    data = multi_line_data()

    sequences = [find_all_numbers(sequence) for sequence in data]

    total = 0
    for sequence in sequences:
        extrapolated = get_extrapolated_data(sequence)
        extrapolated = predict(extrapolated, backwards=True)

        total += extrapolated[0][0]
    return total


def get_extrapolated_data(initial_sequence):
    extrapolated = [initial_sequence]

    last_row = extrapolated[-1]
    while not all(x == 0 for x in last_row):
        differences = [last_row[i + 1] - last_row[i] for i in range(0, len(last_row) - 1)]
        extrapolated.append(differences)
        last_row = differences

    return extrapolated


def predict(extrapolated, backwards=False):
    for sequence_index in range(len(extrapolated) - 1, 0, -1):
        seq = extrapolated[sequence_index]
        seq_up = extrapolated[sequence_index - 1]

        if backwards:
            extrapolated[sequence_index - 1].insert(0, seq_up[0] - seq[0])
        else:
            extrapolated[sequence_index - 1].append(seq[-1] + seq_up[-1])

    return extrapolated


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
