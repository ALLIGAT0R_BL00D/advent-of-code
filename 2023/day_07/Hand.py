letter_cards = 'TJQKA'


class Hand:
    joker = 'J'

    def __init__(self, hand, has_jokers=False):
        cards, number = hand.split(' ')
        self.has_jokers = has_jokers
        self.cards = cards
        self.bid = int(number)
        self.hand = self.determine_hand()
        self.mock_cards = self.mock_cards()

    def determine_hand(self):
        found_cards = get_number_per_card(self.cards if not self.has_jokers else self.get_joker_cards())

        if len(found_cards) == 1:
            return '5 of a kind'

        if len(found_cards) == 2:
            highest_card_count = max(found_cards.values())
            if highest_card_count == 4:
                return '4 of a kind'
            else:
                return 'full house'

        if len(found_cards) == 3:
            highest_card_count = max(found_cards.values())
            if highest_card_count == 3:
                return '3 of a kind'
            else:
                return '2 pair'

        if len(found_cards) == 4:
            return 'pair'

        if len(found_cards) == 5:
            return 'highest'

        return None

    def get_joker_cards(self):
        found_cards = get_number_per_card(self.cards)

        highest_card = (self.joker, 0)
        for card, number in found_cards.items():
            if card == self.joker:
                continue

            if number > highest_card[1]:
                highest_card = (card, number)

            if number == highest_card[1] and get_card_value(card) > get_card_value(highest_card[0]):
                highest_card = (card, number)

        if highest_card[0] == self.joker:
            return 'AAAAA'
        return ''.join(card if card != self.joker else highest_card[0] for card in self.cards)

    def mock_cards(self):
        return ''.join(get_mock_card(card, self.has_jokers) for card in self.cards)


def get_mock_card(card, exclude_joker=False):
    if card not in letter_cards:
        return card
    j = 'B' if not exclude_joker else '.'
    return {'T': 'A', 'J': j, 'Q': 'C', 'K': 'D', 'A': 'E'}[card]


def get_card_value(card):
    if card not in letter_cards:
        return int(card)
    return {'T': 10, 'J': 11, 'Q': 12, 'K': 13, 'A': 14}[card]


def get_number_per_card(cards):
    found_cards = {}
    for card in cards:
        if card in found_cards.keys():
            continue

        number_of_card = cards.count(card)
        found_cards[card] = number_of_card
    return found_cards


def get_winnings(win_hands):
    i = 1
    total = 0
    for key in win_hands.keys():
        for hand in sorted(win_hands[key], key=lambda x: x.mock_cards):
            total += hand.bid * i
            i += 1
    return total
