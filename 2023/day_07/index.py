from Hand import Hand, get_winnings
from helpers.data import multi_line_data, run


def part1():
    data = multi_line_data()

    hands = [Hand(line) for line in data]

    win_hands = {
        'highest': [],
        'pair': [],
        '2 pair': [],
        '3 of a kind': [],
        'full house': [],
        '4 of a kind': [],
        '5 of a kind': [],
    }

    for hand in hands:
        win_hands[hand.hand].append(hand)

    return get_winnings(win_hands)


def part2():
    data = multi_line_data()

    hands = [Hand(line, True) for line in data]

    win_hands = {
        'highest': [],
        'pair': [],
        '2 pair': [],
        '3 of a kind': [],
        'full house': [],
        '4 of a kind': [],
        '5 of a kind': [],
    }

    for hand in hands:
        win_hands[hand.hand].append(hand)

    return get_winnings(win_hands)


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
