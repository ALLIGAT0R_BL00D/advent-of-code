# Day 3: Gear Ratios

## [Part 1](https://adventofcode.com/2023/day/3)

For each row I find the `part numbers` with the position in the row. Then I
look around the `part numbers` so check if they are adjacent to a `symbol`.
When the `part number` is adjacent to a `symbol`, I add the `part number`
to the total

## [Part 2](https://adventofcode.com/2023/day/3#part2)

For each row I find the `gears`. For every `gear`, I create a range to look
around it. Then I look for the `part numbers` in each of the rows in range
of the `gear`. Then I go over these `part numbers` and see if they are
adjacent to the `gear`. When that is the case, I save the number, and when
I end up with two `part numbers`, I multiply them together and add the
result to total
