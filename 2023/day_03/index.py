import re

from helpers.data import multi_line_data, run


def part1():
    data = multi_line_data()

    total = 0
    for i, line in enumerate(data):
        numbers = re.finditer(r'\d+', line)

        for number in numbers:
            num = int(number.group())
            start, end = number.span()

            x_start = start if start == 0 else start - 1
            x_end = end if end == len(line) else end + 1
            y_start = i if i == 0 else i - 1
            y_end = i + 1 if i == len(data) - 1 else i + 2

            x_range = range(x_start, x_end)
            y_range = range(y_start, y_end)

            for row in y_range:
                stop = False
                for col in x_range:
                    if data[row][col] not in '1234567890.':
                        total += num
                        stop = True
                        break
                if stop:
                    break

    return total


def part2():
    data = multi_line_data()

    total = 0
    for row, line in enumerate(data):
        if '*' not in line:
            continue

        for gear in re.finditer(r'\*', line):
            index = gear.start()
            x_range = range(index - 1, index + 2)
            y_range = range(row - 1, row + 2)

            numbers = []
            for x in y_range:
                nums = re.finditer(r'\d+', data[x])
                for y in nums:
                    num = int(y.group())
                    start, end = y.span()

                    if start in x_range or end - 1 in x_range:
                        numbers.append(num)

            if len(numbers) == 2:
                total += numbers[0] * numbers[1]

    return total


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
