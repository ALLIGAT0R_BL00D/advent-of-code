from helpers.data import multi_line_data, run

g = '#'


def part(expansion):
    data = multi_line_data()

    galaxies = []
    image_y = 0
    for row, row_data in enumerate(data):
        image_x = 0
        for col, char in enumerate(row_data):
            if char == g:
                galaxies.append((image_x, image_y))

            if g not in [_[col] for _ in data]:
                image_x += expansion - 1
            image_x += 1

        if g not in row_data:
            image_y += expansion - 1
        image_y += 1

    lengths = []
    for i, galaxy in enumerate(galaxies):
        for other_galaxy_i in range(i + 1, len(galaxies)):
            other_galaxy = galaxies[other_galaxy_i]
            lengths.append(abs(other_galaxy[1] - galaxy[1]) + abs(other_galaxy[0] - galaxy[0]))

    return sum(lengths)


if __name__ == '__main__':
    print(f'part1 solution: {run(part, 2)}')
    print(f'part1 solution: {run(part, 1_000_000)}')
