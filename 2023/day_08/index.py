import math
import re

from helpers.data import multi_line_data, run

node = {'R': 1, 'L': 0}


def part1():
    data = multi_line_data()

    instructions = data[0]
    network = {}

    for i, line in enumerate(data[2::]):
        key, right, left = re.findall(r'\w{3}', line)
        network[key] = (right, left)

    step = 0
    location = 'AAA'
    while location != 'ZZZ':
        direction = instructions[step % len(instructions)]
        location = network[location][node[direction]]
        step += 1

    return step


def part2():
    data = multi_line_data()

    instructions = data[0]
    network = {}

    for i, line in enumerate(data[2::]):
        key, right, left = re.findall(r'\w{3}', line)
        network[key] = (right, left)

    steps = []
    locations = [loc for loc in network.keys() if loc[-1] == 'A']
    for location in locations:
        step = 0

        # every path is a loop, so we only need the number of steps once
        while location[-1] != "Z":
            direction = instructions[step % len(instructions)]
            location = network[location][node[direction]]
            step += 1
        steps.append(step)

    # from all the different steps, we want the first value
    # where the would all hit. Which is the Least Common Multiple
    return math.lcm(*steps)


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
