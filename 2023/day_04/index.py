import re

from helpers.data import multi_line_data, run


def part1():
    data = multi_line_data()

    total = 0
    for line in data:
        _, cards = re.split(r': +', line)
        w, o = re.split(r' \| +', cards)

        winning = [int(x) for x in re.split(r' +', w)]
        own = [int(x) for x in re.split(r' +', o)]

        points = 0
        for num in own:
            if num in winning:
                points += 1 if points == 0 else points
        total += points

    return total


def part2():
    data = multi_line_data()
    cards = {(i + 1): 1 for i, _ in enumerate(data)}

    for line in data:
        card, w, o = re.split(r': +| \| +', line)
        card_id = int(re.findall(r'\d+', card)[0])
        winning = [int(x) for x in re.split(r' +', w)]
        own = [int(x) for x in re.split(r' +', o)]

        won = sum(int(num in winning) for num in own)
        for x in range(won):
            cards[card_id + x + 1] += cards[card_id]

    return sum(cards.values())


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
