# Day 4: Scratchcards

## [Part 1](https://adventofcode.com/2023/day/4)

I start by extracting the `scratchcards` and `winning numbers` in a way
where I can use them. Then I go over `my numbers` and check them against
the `winning numbers`. The first match, I will get `1 point`, and after
that, every win `doubles` my `points`.

## [Part 2](https://adventofcode.com/2023/day/4#part2)

I start by making a map where I keep track of the number of each `card` I
have. Then I go over the cards. Every time I win, I receive an extra `card`
for the next rounds. So, let's say I win `2` times on `card 1`, then I get
and extra attempt on `card 2` and `card 3`. In the end, I lose; None of my
numbers match the `winning numbers`. When this happens, I sum up all
the `scratchcards` I have played
