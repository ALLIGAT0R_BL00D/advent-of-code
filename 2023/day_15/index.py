from helpers.data import run, single_line_data

eq = '='


def part1():
    data = single_line_data('practice')

    return sum(hashmap(h) for h in data.split(','))


def part2():
    data = single_line_data('practice')
    lenses = data.split(',')

    boxes = {}
    for lens in lenses:
        label, operation, focal = None, None, None
        if eq in lens:
            label, number = lens.split(eq)
            operation, focal = eq, int(number)
        else:
            label, operation = lens[0:-1], lens[-1]

        box = hashmap(label)

        if box not in boxes:
            boxes[box] = []

        if operation == eq:
            in_box = any([lbl == label for lbl, _ in boxes[box]])
            if in_box:
                boxes[box] = [(lbl, focal if lbl == label and in_box else f) for lbl, f in boxes[box]]
            else:
                boxes[box].append((label, focal))
        else:
            boxes[box] = [(lbl, f) for lbl, f in boxes[box] if lbl != label]

    total = 0
    for box, lenses in boxes.items():
        total += sum((box + 1) * (i + 1) * f for i, (_, f) in enumerate(lenses))

    return total


def hashmap(chars):
    value = 0
    for char in chars:
        value = ((value + ord(char)) * 17) % 256
    return value


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
