from helpers.data import multi_line_data, run

n, w, s, e = (0, -1), (-1, 0), (0, 1), (1, 0)


def part1():
    data = multi_line_data('practice.txt')

    dish = [[col for col in row] for row in data]

    total = 0
    for y, row in enumerate(dish):
        for x, col in enumerate(row):
            new_y = y
            if dish[y][x] == 'O':
                while new_y > 0 and dish[new_y - 1][x] == ".":
                    new_y -= 1

                dish[y][x] = '.'
                dish[new_y][x] = 'O'

                total += len(dish) - new_y

    return total


def part2():
    data = multi_line_data('practice.txt')

    dish = [[col for col in row] for row in data]

    total = 0
    for _ in range(1):  # 1_000_000_000
        if _ % 1_000_000 == 0:
            print(_)

        for dir_ in [n, w, s, e]:
            checked = set()
            for y, row in enumerate(dish):
                for x, col in enumerate(row):
                    if dish[y][x] == 'O':
                        if (x, y) in checked:
                            continue
                        new_x, new_y = x, y

                        if dir_ == n:
                            checked.add((x, y))
                            rocks = 1
                            dish[y][x] = '.'

                            while new_y < len(dish) - 1 and dish[new_y + 1][new_x] != "#":
                                new_y += 1

                                if dish[new_y][new_x] == 'O':
                                    rocks += 1
                                dish[new_y][new_x] = '.'
                                checked.add((new_x, new_y))

                            new_y = y
                            while new_y > 0 and dish[new_y - 1][x] == ".":
                                new_y -= 1

                            for offset in range(new_y, new_y + rocks):
                                dish[offset][new_x] = 'O'
                        elif dir_ == w:
                            checked.add((x, y))
                            rocks = 1
                            dish[y][x] = '.'

                            while new_x < len(row) - 1 and dish[new_y][new_x + 1] != "#":
                                new_x += 1

                                if dish[new_y][new_x] == 'O':
                                    rocks += 1
                                dish[new_y][new_x] = '.'
                                checked.add((new_x, new_y))

                            new_x = x
                            while new_x > 0 and dish[new_y][new_x - 1] == ".":
                                new_x -= 1

                            for offset in range(new_x, new_x + rocks):
                                dish[new_y][offset] = 'O'
                        elif dir_ == s:
                            checked.add((x, y))
                            rocks = 1
                            dish[y][x] = '.'

                            while new_y < len(dish) - 1 and dish[new_y + 1][new_x] != "#":
                                new_y += 1

                                if dish[new_y][new_x] == 'O':
                                    rocks += 1
                                dish[new_y][new_x] = '.'
                                checked.add((new_x, new_y))

                            for offset in range(new_y, new_y - rocks, -1):
                                dish[offset][new_x] = 'O'
                        elif dir_ == e:
                            checked.add((x, y))
                            rocks = 1
                            dish[y][x] = '.'

                            while new_x < len(row) - 1 and dish[new_y][new_x + 1] != "#":
                                new_x += 1

                                if dish[new_y][new_x] == 'O':
                                    rocks += 1
                                dish[new_y][new_x] = '.'
                                checked.add((new_x, new_y))

                            for offset in range(new_x, new_x - rocks, -1):
                                dish[new_y][offset] = 'O'

        # At some point, you'll get in a loop,
        # this is where you'd try to find that
        # answer: 94585

    for y, row in enumerate(dish):
        total += row.count('O') * (len(dish) - y)
    return total


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
