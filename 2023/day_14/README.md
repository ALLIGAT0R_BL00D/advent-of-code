# Day 14: Parabolic Reflector Dish

## [Part 1](https://adventofcode.com/2023/day/14)

Firs, I create a 2D array of the `dish`. Then I move all the `round rocks`
up. I do this by going through the grid and for every `round rock`, I
'roll' it Until I can't find `empty space` to move to. I'll demonstrate
this below with `O` `(6, 6)`:

```text
   Start    |    steps     |    end 
____________|______________|____________
            |       ∨      |       ∨
O....#....  |  O....#....  |  O....#....
O.OO#....#  |  O.OO#....#  |  O.OO#....#
.....##...  |  .....##...  |  .....##...
OO.#O....O  |  OO.#O^...O  |> OO.#OO...O
.O.....O#.  |  .O...^.O#.  |  .O.....O#.
O.#..O.#.#  |> O.#..X.#.#  |  O.#....#.#
..O..#O..O  |  ..O..#O..O  |  ..O..#O..O
.......O..  |  .......O..  |  .......O..
#....###..  |  #....###..  |  #....###..
#OO..#....  |  #OO..#....  |  #OO..#....
```

When a rock finished 'rolling', I calculate the `load` with the following
formula: `[number of row] - [the row the rock ended on]`

## [Part 2](https://adventofcode.com/2023/day/14#part2)

Where in part 1, you only had to tilt the platform one direction, you have
to tilt the `dish` is all directions now in one `cycle`. I still go through
all the `round rocks`, but the way I find the new position of the rocks is
a bit different now.

When I find a `round rock`, it will **always** be the first of the
direction I'm checking. It takes three steps to position all the rocks on
their new position:

1. Count all the rocks up until the next `square rock` or edge, replace
   them with and `empty space`, and mark that position as checked
2. Roll to the direction until the next `square rock` or edge
3. Put the number of rocks next to each other starting at that point

Here is what that looks like in the second column; and let's go down this
time:

```text
   Start    |    step 1    |    step 2    | step 3 (end)   
____________|______________|______________|______________
            |   ∨          |   ∨          |   ∨          
O....#....  |  O....#....  |  O....#....  |  O....#....  
O.OO#....#  |  O.OO#....#  |  O.OO#....#  |  O.OO#....#  
.....##...  |  .....##...  |  .....##...  |  .....##...  
OO.#O....O  |> O..#O....O  |> OX.#OO...O  |  O..#OO...O  
.O.....O#.  |  .......O#.  |  .∨.....O#.  |  .......O#.  
O.#..O.#.#  |  O.#..O.#.#  |  O∨#....#.#  |  O.#....#.#  
..O..#O..O  |  ..O..#O..O  |  .∨O..#O..O  |  ..O..#O..O  
.......O..  |  .......O..  |  .∨.....O..  |  .O.....O..  
#....###..  |  #....###..  |  #∨...###..  |  #O...###..  
#OO..#....  |  #.O..#....  |  #∨O..#....  |  #OO..#....  
              3 rocks found
```

Why did I do it differently? Well, When you go down or to the right, my
first solution would work. It would stop too early. I can try to skip rock,
but that would introduce quite some complexity. Besides that, it's also
more efficient. Let me do the same if my first solution would work:

```text
   Start    |    steps     |    end 
____________|______________|____________
            |              |        
O....#....  |  O....#....  |  O....#....
O.OO#....#  |  O.OO#....#  |  O.OO#....#
.....##...  |  .....##...  |  .....##...
OO.#O....O  |  OO.#O....O  |  OX.#OO...O
.O.....O#.  |  .X.....O#.  |  .∨.....O#.
O.#..O.#.#  |  O∨#..O.#.#  |  O∨#....#.#
..O..#O..O  |  .∨O..#O..O  |  .∨O..#O..O
.......O..  |  .∨.....O..  |  .O.....O..
#....###..  |  #O...###..  |  #O...###..
#OO..#....  |  #OO..#....  |  #OO..#....
 1 check        6 checks       7 checks
```

> note: I ignore the checks from the 'going through the grid' check. They
> both have the same ones

In total, it takes the first solution `14` checks. My new solution also
does it in `14`. This doesn't sound like much of an improvement. But, Let's
image a `round rock` all the way in the top of that column. The first
solution would get an extra `10` checks - totaling to `24`. While my new
solution is still sitting at a total of `14`. My solution will be constant
with the
`length to the next square roch or edge opposite from the direction the round rock is falling` +
`length to next square rock or edge the round rock is falling to` +
`the number of round rocks that will move`. Whereas the first solution will
be the sum of `all lengths each rock will fall`.

Now that I down go rock by rock, I don't add the new load immediately. I
also don't need to calculate the load after each cycle, only in the end.
The way I do it is similar. I go through all the rows and then
multiple `[number of row] - [the row the rock ended on]`
by `[number of round rocks in row]`

Well... Just run `1000000000` cycles of tilting in all 4 direction and
calculate the new load. Easy, right? No!

That takes **way** too long. I found that at some point the cycles end up
in a loop of `loads`. Let me leave it at that 😂 I haven't figured out how
to find that loop and calculate the end cycle's state to calculate
the `load`
