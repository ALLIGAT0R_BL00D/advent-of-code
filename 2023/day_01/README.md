# Day 1: Trebuchet?!

## [Part 1](https://adventofcode.com/2023/day/1)

I start by getting all the numbers in each string.
The numbers are found with the following regex:
`\d`. This gets every single digit.
Then, I make a combined number from the first and
last number in each list; and sum all of those
combined number together

## [Part 2](https://adventofcode.com/2023/day/1#part2)

For part two, we had to deal with written out numbers. Instead of finding
the numbers with `\d`, we find them by using the following
regex:

```regexp
(?=(\d|one|two|three|four|five|six|seven|eight|nine))
```

I am
using a `positive lookahead`. This looks for every match in the string.
Without it, strings like `oneight`, would only give use `one`, but we
want `one` and `eight`. When I make the combined number, I map the written
out numbers to a digit and the sum the combined numbers.
