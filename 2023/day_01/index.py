import re

from helpers.data import find_single_digits, multi_line_data, run

num_map = {'one': 1, 'two': 2, 'three': 3, 'four': 4, 'five': 5, 'six': 6, 'seven': 7, 'eight': 8, 'nine': 9}


def part1():
    data = multi_line_data()
    numbers = [find_single_digits(line) for line in data]
    return sum(int(str(nums[0]) + str(nums[-1])) for nums in numbers)


def part2():
    data = multi_line_data()
    numbers = [re.findall(r'(?=(\d|{}))'.format('|'.join(num_map.keys())), line) for line in data]
    return sum(int(get_number(nums[0]) + get_number(nums[-1])) for nums in numbers)


def get_number(x: str) -> str:
    if x in num_map.keys():
        return str(num_map[x])
    return x


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
