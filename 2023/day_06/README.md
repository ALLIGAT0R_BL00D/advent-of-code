# Day 6: Wait For It

Both parts are basically the same. So I created a function where all the
magic happens. This magic gives you the number of ways you beat
the `record`. I start by checking the numbers up, starting at `0`. When I
found the first `record` beating `distance`, I stop. Then I do the same
thing, but starting from the most `time` and count down. I do this because
all number within this range will beat the `record`, and this way is more
efficient that going through all the numbers (this is mostly done for part
2 where it's a lot of `time`). For both parts I go thought the `times` and
see which beats the `records` 
