from helpers.data import find_all_positive_numbers, multi_line_data, run


def part1():
    data = multi_line_data()
    time, distance = data[0], data[1]
    time_distance = [(t, d) for t, d in zip(find_all_positive_numbers(time), find_all_positive_numbers(distance))]

    total = 1
    for t, d in time_distance:
        total *= get_wins(t, d)

    return total


def part2():
    data = multi_line_data()
    time, distance = int(data[0].replace(' ', '')[5::]), int(data[1].replace(' ', '')[9::])

    return get_wins(time, distance)


def get_wins(time: int, distance: int):
    wins_up = 0
    for ms in range(0, time + 1):
        if (time - ms) * ms > distance:
            wins_up = ms
            break

    wins_down = 0
    for ms in range(time, wins_up, -1):
        if (time - ms) * ms > distance:
            wins_down = ms + 1  # have to add one because it includes this number
            break

    return wins_down - wins_up


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
