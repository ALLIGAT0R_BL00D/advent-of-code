import re

from helpers.data import multi_line_data, run

r, g, b = 'red', 'green', 'blue'
MAX_VALUE = {r: 12, g: 13, b: 14}


def part1():
    data = multi_line_data()

    total = 0
    for game in data:
        round_, all_sets = game.split(": ")

        round_id = int(re.findall(r'\d+', round_)[0])

        sets = all_sets.split("; ")

        stop = False
        for set_ in sets:
            colors = set_.split(", ")

            for pick in colors:
                number, color = pick.split(" ")
                number = int(number)

                stop = stop or number > MAX_VALUE[color]

            if stop:
                break

        if stop:
            continue

        total += round_id

    return total


def part2():
    data = multi_line_data()

    total = 0
    for game in data:
        _, all_sets = game.split(": ")
        sets = all_sets.split("; ")

        max_set = {r: 0, g: 0, b: 0}
        for set_ in sets:
            colors = set_.split(", ")

            for pick in colors:
                number, color = pick.split(" ")
                number = int(number)

                max_set[color] = max(max_set[color], number)

        total += max_set[r] * max_set[g] * max_set[b]

    return total


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
