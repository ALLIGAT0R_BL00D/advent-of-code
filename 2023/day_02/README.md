# Day 2: Cube Conundrum

## [Part 1](https://adventofcode.com/2023/day/2)

For each `game`, I go through the colors in each `set`. When I find a color
that is used too much, I ignore the game. otherwise, I add the game if to
the total

## [Part 2](https://adventofcode.com/2023/day/2#part2)

Instead of checking if I have enough of a `color`, I take the maximum
number for each color in the `set`. Then I multiply those numbers and add
the result to the total
