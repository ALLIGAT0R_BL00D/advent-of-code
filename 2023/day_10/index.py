from helpers.data import multi_line_data, run

x_axis, y_axis = 0, 1
s, tl, tr, br, bl, sx, sy = 'S', 'F', '7', 'J', 'L', '-', '|'
down_up, right_left, up_down, left_right = [sy, tl, tr], [sx, tl, bl], [sy, br, bl], [sx, tr, br]
directions = [(0, -1), (1, 0), (0, 1), (-1, 0)]


def part1():
    data = multi_line_data()

    start = None
    for y, line in enumerate(data):
        if s in line:
            start = (line.index(s), y)
            break

    nxt = None
    if 0 <= start[y_axis] < len(data) and data[start[y_axis] - 1][start[x_axis]] in down_up:
        nxt = (start[x_axis], start[y_axis] - 1)
    elif 0 <= start[x_axis] < len(data[0]) and data[start[y_axis]][start[x_axis] + 1] in right_left:
        nxt = (start[x_axis] + 1, start[y_axis])
    elif 0 <= start[y_axis] < len(data) and data[start[y_axis] + 1][start[x_axis]] in up_down:
        nxt = (start[x_axis], start[y_axis] + 1)
    elif 0 <= start[x_axis] < len(data[0]) and data[start[y_axis]][start[x_axis] - 1] in left_right:
        nxt = (start[x_axis] - 1, start[y_axis])

    position_1, steps = start, 0
    while nxt is not None:
        new_next = get_new_coordinates(position_1, nxt, data[nxt[y_axis]][nxt[x_axis]])
        position_1, nxt = nxt, new_next
        steps += 1

    return steps // 2


def part2():
    data = multi_line_data()

    start = None
    for y, line in enumerate(data):
        if s in line:
            start = (line.index(s), y)
            break

    nxt = None
    if 0 <= start[x_axis] - 1 < len(data[0]) and data[start[y_axis]][start[x_axis] - 1] in right_left:
        nxt = (start[x_axis] - 1, start[y_axis])
    elif 0 <= start[x_axis] + 1 < len(data[0]) and data[start[y_axis]][start[x_axis] + 1] in left_right:
        nxt = (start[x_axis] + 1, start[y_axis])
    elif 0 <= start[y_axis] + 1 < len(data) and data[start[y_axis] + 1][start[x_axis]] in up_down:
        nxt = (start[x_axis], start[y_axis] + 1)
    elif 0 <= start[y_axis] - 1 < len(data) and data[start[y_axis] - 1][start[x_axis]] in down_up:
        nxt = (start[x_axis], start[y_axis] - 1)

    position = start
    path = []
    while nxt is not None:
        path.append(nxt)
        new_next = get_new_coordinates(position, nxt, data[nxt[y_axis]][nxt[x_axis]])
        position, nxt = nxt, new_next

    check_direction = (0, 0)
    deer = set()
    for i, (x, y) in enumerate(path):
        if (x, y) == start:
            break

        if i == 0:
            y_direction = y - start[y_axis]
        else:
            prev = i - 1
            y_direction = y - path[prev][y_axis]

        if check_direction != (0, 0):
            deer_x, deer_y = x, y
            while (deer_x, deer_y) == (x, y) or \
                    (0 <= deer_x < len(data[0]) and 0 <= deer_y < len(data) and (deer_x, deer_y) not in path):
                deer_x = deer_x + check_direction[x_axis]
                deer_y = deer_y + check_direction[y_axis]

                if (deer_x, deer_y) not in path:
                    deer.add((deer_x, deer_y))

        old_direction = check_direction
        check_direction = get_new_check_direction(old_direction, y_direction, data[y][x])

        if check_direction != (0, 0):
            deer_x, deer_y = x, y
            while (deer_x, deer_y) == (x, y) or \
                    (0 <= deer_x < len(data[0]) and 0 <= deer_y < len(data) and (deer_x, deer_y) not in path):
                deer_x = deer_x + check_direction[x_axis]
                deer_y = deer_y + check_direction[y_axis]

                if (deer_x, deer_y) not in path:
                    deer.add((deer_x, deer_y))

    return len(deer)


def get_new_coordinates(curr, nxt, pipe):
    x_dir, y_dir = nxt[x_axis] - curr[x_axis], nxt[y_axis] - curr[y_axis]

    if pipe == sy:
        return nxt[x_axis], nxt[y_axis] + y_dir
    if pipe == sx:
        return nxt[x_axis] + x_dir, nxt[y_axis]
    if pipe == tl:
        return (nxt[x_axis], nxt[y_axis] + 1) if x_dir == -1 else (nxt[x_axis] + 1, nxt[y_axis])
    if pipe == tr:
        return (nxt[x_axis], nxt[y_axis] + 1) if x_dir == 1 else (nxt[x_axis] - 1, nxt[y_axis])
    if pipe == br:
        return (nxt[x_axis] - 1, nxt[y_axis]) if y_dir == 1 else (nxt[x_axis], nxt[y_axis] - 1)
    if pipe == bl:
        return (nxt[x_axis] + 1, nxt[y_axis]) if y_dir == 1 else (nxt[x_axis], nxt[y_axis] - 1)
    return None  # is start


def get_new_check_direction(curr_direction, y_direction, pipe):
    if pipe in [sx, sy]:
        return curr_direction

    bottom_in, top_in = y_direction == -1, y_direction == 1
    if curr_direction == (0, 0):
        if pipe == tl:
            return (0, 1) if bottom_in else (1, 0)
        if pipe == tr:
            return (0, 1) if bottom_in else (-1, 0)
        if pipe == br:
            return (0, -1) if top_in else (-1, 0)
        if pipe == bl:
            return (0, -1) if top_in else (1, 0)

    if pipe == tl:
        if bottom_in:
            return get_check_direction_clockwise(curr_direction)
        return get_check_direction_counter_clockwise(curr_direction)
    if pipe == tr:
        if bottom_in:
            return get_check_direction_counter_clockwise(curr_direction)
        return get_check_direction_clockwise(curr_direction)
    if pipe == br:
        if top_in:
            return get_check_direction_clockwise(curr_direction)
        return get_check_direction_counter_clockwise(curr_direction)
    if pipe == bl:
        if top_in:
            return get_check_direction_counter_clockwise(curr_direction)
        return get_check_direction_clockwise(curr_direction)


def get_check_direction_clockwise(direction):
    return directions[(directions.index(direction) + 1) % len(directions)]


def get_check_direction_counter_clockwise(direction):
    return directions[directions.index(direction) - 1]


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
