from helpers.data import multi_line_data, run

x_axis, y_axis = 0, 1
s, tl, tr, br, bl, sx, sy = 'S', 'F', '7', 'J', 'L', '-', '|'
down_up, right_left, up_down, left_right = [sy, tl, tr], [sx, tl, bl], [sy, br, bl], [sx, tr, br]

pipe_map = {
    s: '╝',
    tl: '╔',
    tr: '╗',
    br: '╝',
    bl: '╚',
    sx: '═',
    sy: '║',
}


def get_new_coordinates(curr, nxt, pipe):
    x_dir, y_dir = nxt[x_axis] - curr[x_axis], nxt[y_axis] - curr[y_axis]

    if pipe == sy:
        return nxt[x_axis], nxt[y_axis] + y_dir
    if pipe == sx:
        return nxt[x_axis] + x_dir, nxt[y_axis]
    if pipe == tl:
        return (nxt[x_axis], nxt[y_axis] + 1) if x_dir == -1 else (nxt[x_axis] + 1, nxt[y_axis])
    if pipe == tr:
        return (nxt[x_axis], nxt[y_axis] + 1) if x_dir == 1 else (nxt[x_axis] - 1, nxt[y_axis])
    if pipe == br:
        return (nxt[x_axis] - 1, nxt[y_axis]) if y_dir == 1 else (nxt[x_axis], nxt[y_axis] - 1)
    if pipe == bl:
        return (nxt[x_axis] + 1, nxt[y_axis]) if y_dir == 1 else (nxt[x_axis], nxt[y_axis] - 1)
    return None  # is start


def part2_with_ray_casting():
    data = multi_line_data()

    start = None
    for y, line in enumerate(data):
        if s in line:
            start = (line.index(s), y)
            break

    nxt = None
    if 0 <= start[x_axis] - 1 < len(data[0]) and data[start[y_axis]][start[x_axis] - 1] in right_left:
        nxt = (start[x_axis] - 1, start[y_axis])
    elif 0 <= start[x_axis] + 1 < len(data[0]) and data[start[y_axis]][start[x_axis] + 1] in left_right:
        nxt = (start[x_axis] + 1, start[y_axis])
    elif 0 <= start[y_axis] + 1 < len(data) and data[start[y_axis] + 1][start[x_axis]] in up_down:
        nxt = (start[x_axis], start[y_axis] + 1)
    elif 0 <= start[y_axis] - 1 < len(data) and data[start[y_axis] - 1][start[x_axis]] in down_up:
        nxt = (start[x_axis], start[y_axis] - 1)

    position = start
    path = []
    while nxt is not None:
        path.append(nxt)
        new_next = get_new_coordinates(position, nxt, data[nxt[y_axis]][nxt[x_axis]])
        position, nxt = nxt, new_next

    deer = 0
    deer_positions = set()
    for y, row in enumerate(data):
        path_y = [(xx, yy) for xx, yy in path if yy == y]
        x = 0
        while x < len(row):
            possible_deer = 0
            if data[y][x] not in path_y:
                pipe_found, pipes_found = False, 0
                for xx in range(x, len(row)):
                    in_path = (xx, y) in path_y
                    pipe_found = pipe_found or in_path
                    pipes_found += data[y][xx] in '|LJS' and in_path
                    possible_deer += int(not pipe_found)

                if possible_deer > 0 and pipes_found > 0 and pipes_found % 2 == 1:
                    deer += possible_deer
                    for _ in range(possible_deer):
                        deer_positions.add((x + _, y))

            x += possible_deer if possible_deer > 0 else 1

    for y, row in enumerate(data):
        yyy = []
        for x, col in enumerate(row):
            if (x, y) in deer_positions:
                yyy.append('🦌')
            elif (x, y) in path:
                yyy.append(pipe_map[col])
            else:
                yyy.append(' ')
        print(''.join(yyy))

    return deer


if __name__ == '__main__':
    print(f'Ray casting solution: {run(part2_with_ray_casting)}')
