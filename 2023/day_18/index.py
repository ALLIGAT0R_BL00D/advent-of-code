from helpers.data import multi_line_data, run

dirs = {'R': (1, 0), 'D': (0, 1), 'L': (-1, 0), 'U': (0, -1)}
num_dirs = {i: d for i, d in enumerate(dirs.keys())}


def part1():
    data = multi_line_data()

    curr = (0, 0)
    path = {curr}
    for instruction in data:
        direction, number, color = instruction.split(' ')
        number = int(number)

        curr_x, curr_y = curr
        move_x, move_y = dirs[direction]
        for n in range(1, number + 1):
            path.add((curr_x + move_x * n, curr_y + move_y * n))
        curr = (curr_x + move_x * number, curr_y + move_y * number)

    return get_lagoon_area(path)


def part2():
    data = multi_line_data('practice.txt')

    curr = (0, 0)
    path = {curr}
    for instruction in data:
        _, __, hex_ = instruction.split(' ')
        number, direction = hex_[2:-2], int(hex_[-2])
        number = int(number, 16)
        direction = num_dirs[direction]

        curr_x, curr_y = curr
        move_x, move_y = dirs[direction]
        for n in range(1, number + 1):
            path.add((curr_x + move_x * n, curr_y + move_y * n))
        curr = (curr_x + move_x * number, curr_y + move_y * number)

    return get_lagoon_area(path)


def get_lagoon_area(path):
    min_x, min_y = 0, 0
    max_x, max_y = 0, 0
    for x, y in path:
        min_x, min_y = min(min_x, x), min(min_y, y)
        max_x, max_y = max(max_x, x), max(max_y, y)

    path = [(x + abs(min_x), y + abs(min_y)) for x, y in path]

    max_x, max_y = max_x - min_x, max_y - min_y
    area, area_pos = 0, set()

    print(max_x, max_y)
    for y in range(max_y + 1):
        trenches_top = sorted([xx for xx, yy in path if y > 0 and y - 1 == yy])
        trenches_y = sorted([xx for xx, yy in path if yy == y])
        trenches_bottom = sorted([xx for xx, yy in path if y < max_y and y + 1 == yy])
        trenches_in_row = []

        has_top, has_bottom = False, False
        for i, xx in enumerate(trenches_y):
            if i == 0 or trenches_y[i] - trenches_y[i - 1] > 1:
                trenches_in_row.append(xx)
                has_top, has_bottom = xx in trenches_top, xx in trenches_bottom
                continue

            if i == len(trenches_y) - 1 or trenches_y[i + 1] - trenches_y[i] > 1:
                end_has_top, end_has_bottom = xx in trenches_top, xx in trenches_bottom
                if (has_top and end_has_top) or (has_bottom and end_has_bottom):
                    trenches_in_row.append(xx)

        x = 0
        while x < max_x + 1:
            possible_area = 0
            if x not in trenches_y:
                trench_found, trenches_found = False, 0
                xx = x
                while not trench_found and xx < max_x + 1:
                    trench_found = xx in trenches_y

                    if trench_found:
                        break
                    xx += 1

                try:
                    trench_in_trench_groups = trenches_in_row.index(xx)
                except ValueError:
                    trench_in_trench_groups = None

                if trench_in_trench_groups is not None:
                    check_trenches = trenches_in_row[trench_in_trench_groups::]
                    trenches_found = sum(1 for _ in check_trenches)
                    possible_area = xx - x

                if x > 0 and trenches_found > 0 and trenches_found % 2 == 1:
                    area += possible_area
                    for _ in range(possible_area):
                        area_pos.add((x + _, y))

            x += possible_area if possible_area > 0 else 1

    return len(path) + area


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
