from helpers.data import find_all_positive_numbers, multi_line_data, run

sources, destinations = 'sources', 'destinations'


def part1():
    data = multi_line_data()
    seeds = [int(x) for x in find_all_positive_numbers(data[0])]
    data = data[2::]

    maps = {}
    current_map = ''
    for line in data:
        if len(line) == 0:
            continue

        if 'map' in line:
            current_map = line[0:-5]
            maps[current_map] = []
            continue

        source_start, destination_start, range_length = [int(x) for x in find_all_positive_numbers(line)]

        maps[current_map].append({
            sources: range(source_start, source_start + range_length),
            destinations: range(destination_start, destination_start + range_length),
        })

    results = []
    for seed in seeds:
        result = seed
        for value in maps.values():
            for ranges in value:
                if result not in ranges[destinations]:
                    continue

                result = result - ranges[destinations][0] + ranges[sources][0]
                break
        results.append(result)

    return min(results)


def part2():
    data = multi_line_data()
    initial_seeds = [int(x) for x in find_all_positive_numbers(data[0])]
    seeds = initial_seeds[0::2]
    ranges = initial_seeds[1::2]

    data = data[2::]

    maps = {}
    current_map = ''
    for line in data:
        if len(line) == 0:
            continue

        if 'map' in line:
            from_, _, to = line[0:-5].split('-')
            current_map = to
            maps[current_map] = {
                'from': from_,
                'values': []
            }
            continue

        source_start, destination_start, range_length = [int(x) for x in find_all_positive_numbers(line)]

        maps[current_map]['values'].append({
            sources: range(source_start, source_start + range_length),
            destinations: range(destination_start, destination_start + range_length),
        })

    location = 100000000  # to not have it run for 30 minutes
    while True:
        process_step = 'location'
        result = location
        while True:
            map_ = maps[process_step]
            for value in map_['values']:
                if value[sources][0] <= result <= value[sources][-1]:
                    result = result - value[sources][0] + value[destinations][0]
                    break

            if map_['from'] == 'seed':
                break

            process_step = map_['from']

        for i, s in enumerate(seeds):
            if s <= result <= s + ranges[i]:
                return location
        location += 1


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
