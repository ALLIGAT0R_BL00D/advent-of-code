# Day 5: If You Give A Seed A Fertilizer

## [Part 1](https://adventofcode.com/2023/day/5)

I start by creating a map for each step with all the `ranges`. Then for
each `seed`, I look if the seed is in the map. If it is, I map the `seed`
to its new `location`. Otherwise, it will stay where it is. I continue this
process, for all the maps and `locations`. At the end, I get the smallest
number from the final `locations`

## [Part 2](https://adventofcode.com/2023/day/5#part2)

I couldn't come up with a nice solution for this part. But I looked at the
numbers and thought that going bottom up, might be quicker. So, that's what
I made. Starting at `0`, I count up and check all end locations and go up.
When I'm at
the `seed` level, I check if my `seed`, with that given `end location` is
in one of my `seed ranges`.

In the end it took almost `30 minutes` to find my seed. So... very
efficient...? I think not. I have to go back to this and see if I can think
of a more efficient solution. A colleague did share his approach.

<details><summary>Colleague's approach</summary>

> For day 5 instead of looping over every number in the seed ranges, I took
> the first number and the last number of every seed range. And compared
> them
> against the options of the map ranges. And put every thing in a new array
> to do it again voor the next map with the new min and max values of the
> new
> ranges. At the end you got an array with the ranges that come out of the
> calculations, and you take the lowest min value and that is your answer.
>
> Ik check eerst of het begin nummer van de seed range binnen de map range
> ligt. Als dat het geval is kijk in naar het eind nummer. Als die buiten
> de
> eindwaarde van de map range ligt, maak ik dat de nieuwe eind waarde van
> de
> seeds range en maak ik een nieuwe range die ik weer aan de lijst toevoeg
> die nog gecheckt moeten worden.
</details>
