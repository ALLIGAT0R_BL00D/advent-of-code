import re
import time


def run(func, *args):
    """
    :param func: Function that we want to time
    :return: The time it takes to run func
    """
    start = time.time()
    result = func(*args)
    end = time.time()
    return f'{result:10} [{end - start:.2f} sec]'


def single_line_data(path='data'):
    """
    Get the content of a file that is on one line
    :param path: file to get the data from
    :return: String
    """
    with (open('{}.txt'.format(path)) as file):
        data = file.readline()
    return data


def multi_line_data(path='data.txt'):
    """
    Get the content of a file that is multiline
    :param path: file to get the data from
    :return: Array of strings
    """
    with open(path) as file:
        data = [x.strip() for x in file.readlines()]
    return data


def find_single_digits(line):
    """
    :param line: string where the numbers are in
    :return: array with all the digits; only positive
    """
    return [int(n) for n in re.findall(r'\d', line)]


def find_all_positive_numbers(line):
    """
    :param line: string where the numbers are in
    :return: Array with all the positive numbers
    """
    return [int(n) for n in re.findall(r'\d+', line)]


def find_all_numbers(line):
    """
    :param line: String where the numbers are in
    :return: array with all the numbers; both negative and positive
    """
    return [int(n) for n in re.findall(r'-?\d+', line)]


def shift_right(a, b):
    """
    :param a: numerator
    :param b: power

    a >> b is equivalent to a / 2^b

    It has the added benefit of truncating decimals
    """
    return a >> b


def xor(a, b):
    return a ^ b
