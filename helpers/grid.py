from helpers.Direction import Direction


class Grid:
    grid = None
    rows_len = None
    cols_len = None

    def __init__(self, data, is_number_grid=False):
        if is_number_grid:
            self.grid = [[int(col) for col in row] for row in data]
            return
        self.grid = [[col for col in row] for row in data]
        self.rows_len = len(self.grid)
        self.cols_len = len(self.grid[0])

    @property
    def rows(self):
        return enumerate(self.grid)

    def cols(self, row):
        return enumerate(self.grid[row])

    def find(self, char):
        for r, row in self.rows:
            for c, col in self.cols(r):
                if col == char:
                    return r, c
        return None

    def is_in(self, row, col):
        return 0 <= row < len(self.grid) and 0 <= col < len(self.grid[0])

    def get_cell(self, position):
        row, col = position
        return self.grid[row][col]

    def set_cell(self, row, col, value):
        self.grid[row][col] = value

    def remove_cell(self, row, col):
        return self.grid[row].pop(col)

    def add_cell(self, row, col, item):
        self.grid[row].insert(col, item)

    def get_cell_top(self, row, col):
        if self.is_in(row - 1, col):
            return row - 1, col
        return None

    def get_cell_right(self, row, col):
        if self.is_in(row, col + 1):
            return row, col + 1
        return None

    def get_cell_bottom(self, row, col):
        if self.is_in(row + 1, col):
            return row + 1, col
        return None

    def get_cell_left(self, row, col):
        if self.is_in(row, col - 1):
            return row, col - 1
        return None

    def get_neighbors(self, row, col):
        return [coordinate for coordinate in [
            self.get_cell_top(row, col),
            self.get_cell_right(row, col),
            self.get_cell_bottom(row, col),
            self.get_cell_left(row, col),
        ] if coordinate is not None]

    def get_filtered_neighbors(self, row, col, callback):
        return [(r, c) for r, c in self.get_neighbors(row, col) if callback(r, c)]

    def get_neighbors_with_direction(self, row, col):
        return [(coordinate, d) for coordinate, d in [
            (self.get_cell_top(row, col), Direction.UP),
            (self.get_cell_right(row, col), Direction.RIGHT),
            (self.get_cell_bottom(row, col), Direction.DOWN),
            (self.get_cell_left(row, col), Direction.LEFT),
        ] if coordinate is not None]

    def get_filtered_neighbors_with_direction(self, row, col, callback):
        return [(r, c, d) for (r, c), d in self.get_neighbors_with_direction(row, col) if callback(r, c)]

    def show(self, callback):
        for r, row in enumerate(self.grid):
            pr = ''
            for c, col in enumerate(row):
                pr += callback(col, r, c)
            print(pr)
