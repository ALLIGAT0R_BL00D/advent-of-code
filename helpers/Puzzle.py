import os.path


class Puzzle:
    def __init__(self, year, day):
        self.year = str(year)
        self.day = 'day_{:02d}'.format(day)

    @property
    def single_line_data(self):
        with open(self.__path('data')) as file:
            data = file.readline()
        return data

    @property
    def multi_line_data(self):
        with open(self.__path('data')) as file:
            data = [x.strip() for x in file.readlines()]
        return data

    def multi_line_practice(self, num=''):
        with open(self.__path('practice{}'.format(num))) as file:
            data = [x.strip() for x in file.readlines()]
        return data

    def write(self, data=''):
        with open(self.__path('output'), 'a') as file:
            file.write(data + '\n')

    def __path(self, file):
        return os.path.join(self.year, self.day, '{}.txt'.format(file))
