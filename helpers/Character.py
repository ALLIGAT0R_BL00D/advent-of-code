from helpers.Direction import Direction
from helpers.grid import Grid


class Character:
    start: tuple[int, int] = None
    visited: set[tuple[int, int]] = set()
    path: list[tuple[int, int]] = []
    position: tuple[int, int] = None
    direction: Direction = None
    move_map = {
        Direction.UP: (-1, 0),
        Direction.RIGHT: (0, 1),
        Direction.DOWN: (1, 0),
        Direction.LEFT: (0, -1),
    }

    def __init__(self, data):
        self.grid = Grid(data)

    def update_grid(self, grid: Grid):
        self.grid = grid

    def set_start(self, start):
        self.start = start
        self.position = start
        self.visited.add(start)
        self.path.append(start)

    def set_direction(self, direction: Direction):
        self.direction = direction

    def turn_left(self):
        self.direction = {
            Direction.UP: Direction.LEFT,
            Direction.RIGHT: Direction.UP,
            Direction.DOWN: Direction.RIGHT,
            Direction.LEFT: Direction.DOWN,
        }[self.direction]

    def turn_right(self):
        self.direction = {
            Direction.UP: Direction.RIGHT,
            Direction.RIGHT: Direction.DOWN,
            Direction.DOWN: Direction.LEFT,
            Direction.LEFT: Direction.UP,
        }[self.direction]

    def turn_around(self):
        self.direction = {
            Direction.UP: Direction.DOWN,
            Direction.RIGHT: Direction.LEFT,
            Direction.DOWN: Direction.UP,
            Direction.LEFT: Direction.RIGHT,
        }[self.direction]

    def get_tile(self, row, col):
        return self.grid.get_cell((row, col))

    def set_tile(self, row, col, value):
        self.grid.grid[row][col] = value

    def remove_tile(self, row, col):
        return self.grid.grid[row].pop(col)

    def add_tile(self, row, col, item):
        self.grid.grid[row].insert(col, item)

    def look_forward_for(self, find_criteria, stop_criteria):
        position_r, position_c = self.position
        delta_r, delta_c = self.move_map[self.direction]

        row, col = position_r + delta_r, position_c + delta_c
        while self.grid.is_in(row, col):
            if stop_criteria(row, col):
                return None
            if find_criteria(row, col):
                return row, col

            row, col = row + delta_r, col + delta_c

        return None

    def can_move(self, callback):
        position_r, position_c = self.position
        delta_r, delta_c = self.move_map[self.direction]
        new_r, new_c = position_r + delta_r, position_c + delta_c

        return callback(new_r, new_c)

    def get_next_step(self):
        position_r, position_c = self.position
        delta_r, delta_c = self.move_map[self.direction]
        return position_r + delta_r, position_c + delta_c

    def move(self):
        position_r, position_c = self.position
        delta_r, delta_c = self.move_map[self.direction]
        self.position = position_r + delta_r, position_c + delta_c

    def move_while(self, callback):
        while self.can_move(callback):
            self.move()
            self.visited.add(self.position)
            self.path.append(self.position)

    def __str__(self):
        return """Position: {}
Direction {}
        """.format(self.position, self.direction)
