import time


def run(func):
    start = time.time()
    result = func()
    end = time.time()
    return f'{result:10} [{end - start:.2f} sec]'
