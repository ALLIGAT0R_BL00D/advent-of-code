from helpers.time import run


def part1():
    with open('data.txt') as file:
        data = file.readlines()

    last_number = None
    total = 0
    for x in data:
        if last_number is not None and int(x) > last_number:
            total += 1
        last_number = int(x)

    return total


def part2():
    with open('data.txt') as file:
        data = file.readlines()

    last_measurement = None
    total = 0
    for i, x in enumerate(data):
        measurement = int(x) + int(data[i + 1]) + int(data[i + 2])
        if last_measurement is not None and measurement > last_measurement:
            total += 1
        last_measurement = measurement

        if i + 3 == len(data):
            return total


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
