from helpers.time import run


def part1():
    with open('data.txt') as file:
        data = file.readlines()

    totals = {
        'forward': 0,
        'up': 0,
        'down': 0
    }

    for row in data:
        action, amount = row.split(' ')

        totals[action] += int(amount)

    return totals['forward'] * (totals['down'] - totals['up'])


def part2():
    with open('data.txt') as file:
        data = file.readlines()

    totals = {
        'forward': 0,
        'depth': 0,
        'up': 0,
        'down': 0
    }

    for row in data:
        action, amount = row.split(' ')

        totals[action] += int(amount)

        if action == 'forward':
            totals['depth'] += (totals['down'] - totals['up']) * int(amount)

    return totals['forward'] * totals['depth']


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
