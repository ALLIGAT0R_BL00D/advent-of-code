import re

from helpers.time import run


def part1():
    max_score = 1000
    with open('data.txt') as file:
        data = [{'pos': int(re.findall(r'\d+', x)[1]), 'score': 0} for x in file.readlines()]

    rolls = 0
    while data[0]['score'] < max_score and data[1]['score'] < max_score:
        index = rolls % len(data)

        new_pos = data[index]['pos'] + (3 * (3 * rolls + 2))

        if new_pos % 10 == 0:
            new_pos = 10
        else:
            new_pos %= 10

        data[index]['pos'] = new_pos
        data[index]['score'] += new_pos

        rolls += 1
    print(data)
    return min(data[0]['score'], data[1]['score']) * (rolls * 3)


def part2():
    return -1


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
