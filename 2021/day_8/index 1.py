from helpers.time import run


def part1():
    output1478 = [2, 4, 3, 7]  # amount of segments used
    with open('data.txt') as file:
        data = [x.strip().split(' | ') for x in file.readlines()]

    unique = []
    for _, output in data:
        unique += [x for x in output.split(' ') if len(x) in output1478]
    return len(unique)


# order:
# 3, 9, 0, 6, 5, 2
def part2():
    output1478 = {1: 2, 4: 4, 7: 3, 8: 7}  # amount of segments used
    with open('data.txt') as file:
        data = [x.strip().split(' | ') for x in file.readlines()]

    total = 0
    for input_, output in data:
        inputs, outputs = input_.split(' '), output.split(' ')
        digits = {}

        # add known unique digits
        for i in inputs:
            for k, v in output1478.items():
                if len(i) == v:
                    digits[k] = i

        # 3, 9, and 0 all have 1 in them
        three_nine_zero = [x for x in inputs if
                           x not in digits.values() and
                           digits[1][0] in x and
                           digits[1][1] in x
                           ]

        # get 3 by looking at the only code with length 5 of 3, 9, and 0
        for d in three_nine_zero:
            if len(d) == 5:
                digits[3] = d
                three_nine_zero.remove(d)

        # get 9 and 0 by checking if 3 is part of the code
        for d in three_nine_zero:
            number = 9 if sum(1 for _ in d if _ in digits[3]) == len(d) - 1 else 0
            digits[number] = d

        # get 6 by getting the only remaining code with length of 6
        digits[6] = [d for d in inputs if d not in digits.values() and len(d) == 6][0]

        # get 5 by checking if the code fits in 6 (all of 5 are in 6). 2 is left over
        two_five = [d for d in inputs if d not in digits.values() and len(d) == 5]
        for d in two_five:
            number = 5 if sum(1 for _ in d if _ in digits[6]) == len(d) else 2
            digits[number] = d

        # change output to the digits
        for i, o in enumerate(outputs):
            for k, v in digits.items():
                found = len([_ for _ in o if _ in v]) == len(v) == len(o)
                if found:
                    outputs[i] = str(k)

        # join the output digits and add to total
        total += int(''.join(outputs))

    return total


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
