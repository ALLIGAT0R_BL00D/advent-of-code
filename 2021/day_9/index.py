from helpers.time import run


def part1():
    with open('data.txt') as file:
        data = [[int(x) for x in row.strip()] for row in file.readlines()]

    lowest = []
    for i, row in enumerate(data):
        for j, col in enumerate(row):
            if (i == 0 or col < data[i - 1][j]) and \
                    (i == len(data) - 1 or col < data[i + 1][j]) and \
                    (j == 0 or col < data[i][j - 1]) and \
                    (j == len(row) - 1 or col < data[i][j + 1]):
                lowest.append(col)
    return sum(lowest) + len(lowest)


def part2():
    with open('data.txt') as file:
        data = [[int(x) for x in row.strip()] for row in file.readlines()]

    something = []
    for i, _ in enumerate(data):
        for j, __ in enumerate(_):
            something.append(flood_fill(data, i, j))

    highest = sorted(something)[-3:]
    return highest[0] * highest[1] * highest[2]


def flood_fill(data, r, c):
    # outside grid or nine
    if r < 0 or r == len(data) or c < 0 or c == len(data[r]) or data[r][c] == 9:
        return 0

    # set as visited
    data[r][c] = 9

    return sum([flood_fill(data, r + 1, c),
                flood_fill(data, r - 1, c),
                flood_fill(data, r, c + 1),
                flood_fill(data, r, c - 1)]) + 1


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
