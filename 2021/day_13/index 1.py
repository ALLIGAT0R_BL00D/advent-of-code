from helpers.time import run


def part1(p1=True):
    with open('data.txt') as file:
        data = [_.strip() for _ in file.readlines()]

    instructions, dots = [], []
    for i, line in enumerate(data):
        if line == '':
            continue

        if not line.startswith('fold'):
            x, y = [int(x) for x in line.split(',')]
            dots.append([x, y])
            continue

        axis, fold_line = line[11:].split('=')
        instructions.append((axis, fold_line))

    w, h = 999, 999
    for axis, line in instructions:
        line = int(line)

        dots_copy = dots[:]
        for i, [x, y] in enumerate(dots_copy):
            if (axis == 'x' and line == x) or (axis == 'y' and line == y):
                dots_copy.pop(i)
            if axis == 'x' and line < x:
                dots_copy[i] = [line - (x - line), y]
                w = min(w, line)
            if axis == 'y' and line < y:
                dots_copy[i] = [x, line - (y - line)]
                h = min(h, line)
        dots = dots_copy[:]

        if p1:
            break
    if not p1:
        print_grid(dots, w, h)

    return len(set([(x, y) for x, y in dots]))


def part2():
    return part1(False)


def print_grid(dots, w, h):
    grid = [[[x, y] in dots for x in range(w)] for y in range(h)]
    for _ in grid:
        print(''.join('#' if x else ' ' for x in _))


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
