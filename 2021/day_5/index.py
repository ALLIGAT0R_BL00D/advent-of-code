import re

from helpers.time import run


def part1():
    with open('data.txt') as file:
        data = file.readlines()

    coords = {}
    for line in data:
        x1, y1, x2, y2 = [int(x) for x in re.findall(r'\d+', line)]

        # diagonal
        if x1 != x2 and y1 != y2:
            continue

        range_x = range(x1, x2 + get_step(x1, x2), get_step(x1, x2))
        range_y = range(y1, y2 + get_step(y1, y2), get_step(y1, y2))

        # horizontal
        if y1 == y2:
            for x in range_x:
                if (x, y1) not in coords.keys():
                    coords[(x, y1)] = 0
                coords[(x, y1)] += 1

        # vertical
        if x1 == x2:
            for y in range_y:
                if (x1, y) not in coords.keys():
                    coords[(x1, y)] = 0
                coords[(x1, y)] += 1

    return len([x for x in coords.values() if x >= 2])


def part2():
    with open('data.txt') as file:
        data = file.readlines()

    coords = {}
    for line in data:
        x1, y1, x2, y2 = [int(x) for x in re.findall(r'\d+', line)]

        range_x = range(x1, x2 + get_step(x1, x2), get_step(x1, x2))
        range_y = range(y1, y2 + get_step(y1, y2), get_step(y1, y2))

        # horizontal
        if y1 == y2:
            for x in range_x:
                if (x, y1) not in coords.keys():
                    coords[(x, y1)] = 0
                coords[(x, y1)] += 1
            continue

        # vertical
        if x1 == x2:
            for y in range_y:
                if (x1, y) not in coords.keys():
                    coords[(x1, y)] = 0
                coords[(x1, y)] += 1
            continue

        # diagonal
        for x, y in zip(range_x, range_y):
            if (x, y) not in coords.keys():
                coords[(x, y)] = 0
            coords[(x, y)] += 1

    return len([x for x in coords.values() if x >= 2])


def get_step(a, b):
    return 1 if b > a else -1


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
