from helpers.time import run


def part1():
    with open('data.txt') as file:
        data = [line.strip() for line in file.readlines()]

    points = {')': 3, ']': 57, '}': 1197, '>': 25137}
    start_chunk = {')': '(', ']': '[', '}': '{', '>': '<'}
    illegal_points = []
    for line in data:
        stack = []
        for char in line:
            if char in ')]}>' and start_chunk[char] != stack[-1]:
                illegal_points.append(points[char])
                break

            if char in start_chunk.keys() and start_chunk[char] == stack[-1]:
                stack.pop(-1)
                continue
            stack.append(char)
    return sum(illegal_points)


def part2():
    with open('data.txt') as file:
        data = [line.strip() for line in file.readlines()]

    end_chunk = {'(': ')', '[': ']', '{': '}', '<': '>'}
    fixes_points = []
    for line in data:
        stack = []
        valid = True
        for char in line:
            if char in ')]}>' and end_chunk[stack[-1]] != char:
                valid = False
                break

            if char in end_chunk.values() and end_chunk[stack[-1]] == char:
                stack.pop(-1)
                continue
            stack.append(char)
        if valid:
            fixes_points.append(get_score([end_chunk[_] for _ in stack[::-1]]))
    return sorted(fixes_points)[len(fixes_points) // 2]


def get_score(fix):
    points = {')': 1, ']': 2, '}': 3, '>': 4}
    score = 0
    for x in fix:
        score *= 5
        score += points[x]
    return score


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
