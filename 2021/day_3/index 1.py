from helpers.time import run


def part1():
    with open('data.txt') as file:
        data = file.readlines()

    gamma, epsilon = '', ''
    for i in range(len(data[0]) - 1):
        most_common = int(sum([int(row[i]) for row in data]) > len(data) // 2)
        gamma += str(most_common)
        epsilon += str(1 - most_common)

    return int(gamma, base=2) * int(epsilon, base=2)


def part2():
    with open('data.txt') as file:
        data = file.readlines()

    oxygen, co2 = get_req(data, 0, 'oxygen'), get_req(data, 0, 'co2')

    return int(oxygen, base=2) * int(co2, base=2)


def get_req(data, i, t, val=''):
    if i == len(data[0]) - 1:
        return val

    zeros = [row for row in data if int(row[i]) == 0]
    ones = [row for row in data if int(row[i]) == 1]

    check_oxygen = len(zeros) > len(ones)
    no_ones = len(ones) == 0
    check_co2 = no_ones or len(zeros) <= len(ones) and len(zeros) != 0
    more_zero = (t == 'oxygen' and check_oxygen) or \
                (t == 'co2' and check_co2)

    if more_zero:
        return f'{get_req(zeros, i + 1, t, val)}0'
    else:
        return f'{get_req(ones, i + 1, t, val)}1'


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
