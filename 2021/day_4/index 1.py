import re

from helpers.time import run


def part1():
    with open('data.txt') as file:
        data = file.readlines()

    numbers = [int(x) for x in data[0].strip().split(',')]
    cards = get_cards(data[2:])

    # find number in all grids and calculate the winning card result
    val, checked = 0, 1
    for x in numbers:
        for g, grid in enumerate(cards):
            for r, row in enumerate(grid):
                for c, col in enumerate(row):
                    if x == col[val]:
                        cards[g][r][c][checked] = True

                    if sum([row_[c][checked] for row_ in cards[g]]) == 5:
                        return calc_card(cards, g, x)

                if sum(row_[checked] for row_ in cards[g][r]) == 5:
                    return calc_card(cards, g, x)


def part2():
    with open('data.txt') as file:
        data = file.readlines()

    numbers = [int(x) for x in data[0].strip().split(',')]
    cards = get_cards(data[2:])

    # find number in all grids and calculate the winning card result
    val, checked = 0, 1
    for x in numbers:
        for g, grid in enumerate(cards):
            if cards[g] is None:
                continue

            for r, row in enumerate(grid):
                for c, col in enumerate(row):
                    if cards[g] is None:
                        continue

                    if x == col[val]:
                        cards[g][r][c][checked] = True

                    if sum([row_[c][checked] for row_ in cards[g]]) == 5:
                        if is_last_card(cards):
                            return calc_card(cards, g, x)
                        cards[g] = None

                if cards[g] is None:
                    continue

                if sum(col_[checked] for col_ in cards[g][r]) == 5:
                    if is_last_card(cards):
                        return calc_card(cards, g, x)
                    cards[g] = None


def get_cards(data):
    card, cards = 0, []
    for row in data:
        row = row.strip()
        if row == '':
            card += 1
            continue

        if len(cards) == card:
            cards.append([])

        cards[card].append([[int(x), False] for x in re.split(' +', row)])
    return cards


def calc_card(cards, grid, number, value=0, checked=1):
    return number * sum([
        sum([
            a[value] for a in _ if not a[checked]
        ]) for _ in cards[grid]
    ])


def is_last_card(cards):
    return len([c for c in cards if c is not None]) == 1


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
