from helpers.time import run


def part1(fn=(lambda steps: steps)):
    with open('data.txt') as file:
        data = [int(_) for _ in file.readline().split(',')]

    return min(
        [sum(fn(abs(pos - x)) for pos in data) for x in range(max(data))]
    )


def part2():
    return part1(lambda steps: (steps * (steps + 1)) // 2)


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
