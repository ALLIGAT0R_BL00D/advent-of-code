from helpers.time import run


def part1(days=18):
    life_span = 6
    with open('data.txt') as file:
        data = [int(_) for _ in file.readline().split(',')]

    day, lives = 0, [0 for _ in range(9)]

    for _ in data:
        lives[_] += 1

    while day < days:
        new_fish = lives.pop(0)
        lives[life_span] += new_fish
        lives.append(new_fish)
        day += 1

    return sum(lives)


def part2():
    return part1(256)


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
