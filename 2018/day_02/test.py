import unittest

from helpers.Puzzle import Puzzle
from .index import part1, part2
from helpers.data import multi_line_data


class Year2018Day02Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2018, 2)

    def test_p1_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(part1(data), 12)

    def test_p2_case_1(self):
        data = self.puzzle.multi_line_practice(2)
        self.assertEqual(part2(data), 'fgij')


if __name__ == '__main__':
    unittest.main()
