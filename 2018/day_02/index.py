from helpers.data import run, multi_line_data


def part1(data):
    twos, threes = 0, 0

    for box_id in data:
        count = {}

        for letter in box_id:
            if letter not in count:
                count[letter] = 0
            count[letter] += 1

        twos += 2 in count.values()
        threes += 3 in count.values()

    return twos * threes


def part2(data):
    for i, box_id in enumerate(data):
        for j in range(i + 1, len(data) - 1):
            other_box_id = data[j]

            intersection = [letter for letter, other_letter in zip(box_id, other_box_id) if letter == other_letter]

            if len(intersection) == len(box_id) - 1:
                return ''.join(intersection)
    return None


if __name__ == '__main__':
    input_data = multi_line_data('data.txt')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
