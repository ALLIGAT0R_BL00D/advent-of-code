import unittest
from .index import part1, part2


class Year2018Day01Test(unittest.TestCase):
    def test_p1_case_1(self):
        data = '+1, -2, +3, +1'.split(', ')
        self.assertEqual(part1(data), 3)

    def test_p1_case_2(self):
        data = '+1, +1, +1'.split(', ')
        self.assertEqual(part1(data), 3)

    def test_p1_case_3(self):
        data = '+1, +1, -2'.split(', ')
        self.assertEqual(part1(data), 0)

    def test_p1_case_4(self):
        data = '-1, -2, -3'.split(', ')
        self.assertEqual(part1(data), -6)

    def test_p2_case_1(self):
        data = '+1, -2, +3, +1'.split(', ')
        self.assertEqual(part2(data), 2)

    def test_p2_case_2(self):
        data = '+1, -1'.split(', ')
        self.assertEqual(part2(data), 0)

    def test_p2_case_3(self):
        data = '+3, +3, +4, -2, -4'.split(', ')
        self.assertEqual(part2(data), 10)

    def test_p2_case_4(self):
        data = '-6, +3, +8, +5, -6'.split(', ')
        self.assertEqual(part2(data), 5)

    def test_p2_case_5(self):
        data = '+7, +7, -2, -7, -4'.split(', ')
        self.assertEqual(part2(data), 14)


if __name__ == '__main__':
    unittest.main()
