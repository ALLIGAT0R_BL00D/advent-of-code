from helpers.data import run, multi_line_data, find_all_numbers


def part1(data):
    line = ' '.join(data)
    return sum(find_all_numbers(line))


def part2(data):
    data = [int(x) for x in data]
    frequencies = {0}

    frequency = 0
    i = 0
    while True:
        frequency += data[i % len(data)]

        if frequency in frequencies:
            break

        frequencies.add(frequency)
        i += 1

    return frequency


if __name__ == '__main__':
    input_data = multi_line_data('data.txt')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
