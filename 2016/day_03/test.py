import unittest

from helpers.Puzzle import Puzzle
from .index import part1, part2


class Year2016Day03Test(unittest.TestCase):
    def test_p1_case_1(self):
        data = ['5 10 25']
        self.assertEqual(part1(data), 0)

    def test_p2_case_1(self):
        data = [
            '101 301 501',
            '102 302 502',
            '103 303 503',
            '201 401 601',
            '202 402 602',
            '203 403 603',
        ]
        self.assertEqual(part2(data), 6)


if __name__ == '__main__':
    unittest.main()
