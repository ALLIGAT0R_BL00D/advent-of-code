from helpers.data import run, multi_line_data, find_all_numbers


###################################################
##################### HELPERS #####################
###################################################

def is_possible(a, b, c):
    return a + b > c and b + c > a and a + c > b


###################################################
###################### PARTS ######################
###################################################

def part1(data):
    total = 0
    for row in data:
        total += is_possible(*find_all_numbers(row))

    return total


def part2(data):
    total = 0
    for i in range(0, len(data), 3):
        a1, b1, c1 = find_all_numbers(data[i])
        a2, b2, c2 = find_all_numbers(data[i + 1])
        a3, b3, c3 = find_all_numbers(data[i + 2])

        total += is_possible(a1, a2, a3) + is_possible(b1, b2, b3) + is_possible(c1, c2, c3)
    return total


if __name__ == '__main__':
    input_data = multi_line_data('data.txt')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
