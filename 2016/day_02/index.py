from helpers.data import run, multi_line_data


###################################################
##################### HELPERS #####################
###################################################

def move(position, direction, size):
    return {
        'U': (position[0], max(position[1] - 1, 0)),
        'R': (min(size - 1, position[0] + 1), position[1]),
        'D': (position[0], min(size - 1, position[1] + 1)),
        'L': (max(position[0] - 1, 0), position[1])
    }[direction]


def can_move(numpad, position, direction):
    new_pos = {
        'U': (position[0], max(position[1] - 1, 0)),
        'R': (min(len(numpad) - 1, position[0] + 1), position[1]),
        'D': (position[0], min(len(numpad) - 1, position[1] + 1)),
        'L': (max(position[0] - 1, 0), position[1])
    }[direction]

    return numpad[new_pos[1]][new_pos[0]] is not None


###################################################
###################### PARTS ######################
###################################################

def part1(data):
    numpad = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
    ]
    position = (1, 1)

    code = ''
    for instructions in data:
        for direction in instructions:
            position = move(position, direction, len(numpad))
        code += str(numpad[position[1]][position[0]])

    return code


def part2(data):
    numpad = [
        [None, None, 1, None, None],
        [None, 2, 3, 4, None],
        [5, 6, 7, 8, 9],
        [None, 'A', 'B', 'C', None],
        [None, None, 'D', None, None],
    ]
    position = (0, 2)

    code = ''
    for instructions in data:
        for direction in instructions:
            if can_move(numpad, position, direction):
                position = move(position, direction, len(numpad))
        code += str(numpad[position[1]][position[0]])

    return code


if __name__ == '__main__':
    # input_data = single_line_data('data')
    input_data = multi_line_data('data.txt')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
