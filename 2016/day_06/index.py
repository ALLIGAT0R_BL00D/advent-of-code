from helpers.data import run, multi_line_data


###################################################
##################### HELPERS #####################
###################################################

def get_most_occurring_letter(string):
    unique_letters = set([x for x in string])

    final_letter, count = '', 0
    for letter in unique_letters:
        letter_occurring = string.count(letter)

        if letter_occurring > count:
            final_letter, count = letter, letter_occurring

    return final_letter


def get_least_occurring_letter(string):
    unique_letters = set([x for x in string])

    final_letter, count = '', 999
    for letter in unique_letters:
        letter_occurring = string.count(letter)

        if letter_occurring < count:
            final_letter, count = letter, letter_occurring

    return final_letter


###################################################
###################### PARTS ######################
###################################################

def part1(data):
    grid = zip(*data)

    message = ''
    for row in grid:
        message += get_most_occurring_letter(row)

    return message


def part2(data):
    grid = zip(*data)

    message = ''
    for row in grid:
        message += get_least_occurring_letter(row)

    return message


if __name__ == '__main__':
    # input_data = single_line_data('data')
    input_data = multi_line_data('data.txt')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
