import unittest

from helpers.Puzzle import Puzzle
from helpers.data import multi_line_data
from .index import part1, part2


class YearDayTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2016, 6)

    def test_p1_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(part1(data), 'easter')

    def test_p2_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(part2(data), 'advent')


if __name__ == '__main__':
    unittest.main()
