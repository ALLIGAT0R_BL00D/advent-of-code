from helpers.data import run, single_line_data

dirs = {0: 1, 90: 1, 180: -1, 270: -1}


def part1(data):
    coords = [0, 0]
    dir, axis = 0, 0
    for action in data.split(', '):
        turn, amount = action[0], int(action[1:])

        dir += 90 if turn == 'R' else -90
        coords[axis] += amount * dirs[dir % 360]
        axis = 1 - axis

    return sum(abs(x) for x in coords)


def part2(data):
    coords = [0, 0]
    dir, axis = 0, 0
    visited = set()
    for action in data.split(', '):
        turn, amount = action[0], int(action[1:])

        dir += 90 if turn == 'R' else -90

        for x in range(amount):
            coords[axis] += 1 * dirs[dir % 360]

            coord_tuple = (coords[0], coords[1])
            if coord_tuple in visited:
                return sum(abs(x) for x in coords)

            visited.add(coord_tuple)

        axis = 1 - axis


if __name__ == '__main__':
    input_data = single_line_data()

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
