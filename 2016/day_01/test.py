import unittest

from helpers.Puzzle import Puzzle
from helpers.data import single_line_data
from .index import part1, part2


class Year2016Day01Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2016, 1)

    def test_p1(self):
        data = self.puzzle.single_line_data
        self.assertEqual(part1(data), 262)

    def test_p2(self):
        data = self.puzzle.single_line_data
        self.assertEqual(part2(data), 131)


if __name__ == '__main__':
    unittest.main()
