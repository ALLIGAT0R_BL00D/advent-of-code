import unittest

from helpers.Puzzle import Puzzle
from .index import part1, decrypt_name


class Year2016Day04Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2016, 4)

    def test_p1_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(part1(data), 1514)

    def test_p2_case_1(self):
        self.assertEqual(decrypt_name('qzmt-zixmtkozy-ivhz-', 343), 'very encrypted name')


if __name__ == '__main__':
    unittest.main()
