import re

from helpers.data import run, multi_line_data


###################################################
##################### HELPERS #####################
###################################################

def get_real_rooms(data):
    real_rooms = []
    for room in data:
        name, sector_id, checksum = re.findall(r'(.+(?=\d{3}))(\d+)(?=\[(\w{5}))', room)[0]

        char_map = {}
        for char in name:
            if char == '-':
                continue
            if char not in char_map:
                char_map[char] = 0
            char_map[char] += 1

        letter_sorted = sorted([(char, count) for char, count in char_map.items()], key=lambda x: x[0])
        count_sorted = sorted(letter_sorted, key=lambda x: x[1], reverse=True)
        order = ''.join([letter for letter, _ in count_sorted])
        if ''.join(order).startswith(checksum):
            real_rooms.append((name, int(sector_id), checksum))
    return real_rooms


def decrypt_name(name, sector_id):
    offset = ord('a')
    decrypted_name = ''
    for letter in name:
        decrypted_name += chr((((ord(letter) - offset) + sector_id) % 26) + offset) if letter != '-' else ' '
    return decrypted_name.rstrip()


###################################################
###################### PARTS ######################
###################################################

def part1(data):
    real_rooms = get_real_rooms(data)
    return sum(sector_id for _, sector_id, __ in real_rooms)


def part2(data):
    rooms = get_real_rooms(data)

    for name, sector_id, _ in rooms:
        if decrypt_name(name, sector_id) == 'northpole object storage':
            return sector_id


if __name__ == '__main__':
    input_data = multi_line_data('data.txt')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
