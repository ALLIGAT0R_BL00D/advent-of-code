# 21/10/2021
from helpers.time import run


def part1():
    return -1


# 21/10/2021
def part2():
    return -1


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
