# 21/10/2021
from helpers.time import run


def part1():
    vertical = 0
    horizontal = 0
    visited = {(horizontal, vertical)}

    with open('data.txt') as file:
        data = file.readline()

    for x in data:
        horizontal, vertical = new_house(x, horizontal, vertical)
        visited.add((horizontal, vertical))

    return len(visited)


# 21/10/2021
def part2():
    santa_h, santa_v, robot_h, robot_v = 0, 0, 0, 0
    visited = {(santa_h, santa_v)}

    with open('data.txt') as file:
        data = file.readline()
        santa_route = data[::2]
        robot_route = data[1::2]

    for x in santa_route:
        santa_h, santa_v = new_house(x, santa_h, santa_v)
        visited.add((santa_h, santa_v))

    for x in robot_route:
        robot_h, robot_v = new_house(x, robot_h, robot_v)
        visited.add((robot_h, robot_v))

    return len(visited)


def new_house(d, h, v):
    if d == '^':
        v += 1
    elif d == 'v':
        v -= 1
    elif d == '>':
        h += 1
    elif d == '<':
        h -= 1
    return h, v


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
