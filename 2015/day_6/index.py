import re
from helpers.time import run


# 22/10/2021
def part1():
    lights = [[False for _ in range(1000)] for __ in range(1000)]

    with open('data.txt') as file:
        data = file.readlines()

    for row in data:
        coords = re.findall(r'\d+,\d+', row)
        start_x, start_y = [int(_) for _ in coords[0].split(',')]
        end_x, end_y = [int(_) for _ in coords[1].split(',')]

        for x in range(start_x, end_x + 1):
            for y in range(start_y, end_y + 1):
                if row.startswith('turn on'):
                    lights[x][y] = True
                elif row.startswith('toggle'):
                    lights[x][y] = not lights[x][y]
                elif row.startswith('turn off'):
                    lights[x][y] = False

    return sum(sum([int(_) for _ in row]) for row in lights)


# 22/10/2021
def part2():
    lights = [[0 for _ in range(1000)] for __ in range(1000)]

    with open('data.txt') as file:
        data = file.readlines()

    for row in data:
        coords = re.findall(r'\d+,\d+', row)
        start_x, start_y = [int(_) for _ in coords[0].split(',')]
        end_x, end_y = [int(_) for _ in coords[1].split(',')]

        for x in range(start_x, end_x + 1):
            for y in range(start_y, end_y + 1):
                if row.startswith('turn on'):
                    lights[x][y] += 1
                elif row.startswith('toggle'):
                    lights[x][y] += 2
                elif row.startswith('turn off'):
                    lights[x][y] = max(0, lights[x][y] - 1)

    return sum(sum([int(_) for _ in row]) for row in lights)


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
