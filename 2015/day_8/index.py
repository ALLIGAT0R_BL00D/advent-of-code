from helpers.time import run


# 28/10/2021
def part1():
    with open('data.txt') as file:
        data = file.readlines()

    return sum(len(string[:-1]) - len(eval(string)) for string in data)


# 28/10/2021
def part2():
    with open('data.txt') as file:
        data = file.readlines()

    return sum(2 + string.count('\\') + string.count('"') for string in data)


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
