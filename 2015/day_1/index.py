# 21/10/2021
from helpers.time import run


def part1():
    step = {'(': 1, ')': -1}
    with open('data.txt', 'r') as file:
        data = file.readline()
    return sum([step[x] for x in data])


# 21/10/2021
def part2():
    step = {'(': 1, ')': -1}
    with open('data.txt', 'r') as file:
        data = file.readline()

    floor = 0
    for (i, x) in enumerate(data):
        floor += step[x]
        if floor == -1:
            return i + 1


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
