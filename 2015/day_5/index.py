import re

# 21/10/2021
from helpers.time import run


def part1():
    vowels = ['a', 'e', 'i', 'o', 'u']
    forbidden = ['ab', 'cd', 'pq', 'xy']

    with open('data.txt') as file:
        data = file.readlines()

    nice = 0
    for string in data:
        vowel_count, doubles, invalid = 0, False, False
        for i, letter in enumerate(string):
            vowel_count += 1 if letter in vowels else 0

            if i == 0:
                continue

            prev_letter = string[i - 1]
            combi = f'{prev_letter}{letter}'

            if combi in forbidden:
                invalid = True
                break

            doubles = doubles or letter == prev_letter

        nice += bool(vowel_count >= 3 and doubles and not invalid)

    return nice


# 22/10/2021
def part2():
    with open('data.txt') as file:
        data = file.readlines()

    nice = 0
    for string in data:
        prev = None
        valid = [False, False]
        for i, letter in enumerate(string):
            if prev is None:
                prev = letter
                continue

            # find alternating sequence like: xyx, lql, ama
            combi = f'{prev}{letter}'
            alternate = re.findall(f'{letter}[^{letter}]{letter}', string)
            if len(alternate) > 0:
                valid[0] = True

            # find not overlapping pairs: qpqp, wwlww, aduvuyvad
            pairs = re.findall(f'{combi}.*{combi}', string)
            if len(pairs) > 0:
                valid[1] = True

            prev = letter

        nice += bool(all(valid))

    return nice


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
