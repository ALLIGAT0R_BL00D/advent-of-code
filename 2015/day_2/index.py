# 21/10/2021
from helpers.time import run


def part1():
    with open('data.txt') as file:
        data = file.readlines()

    return sum([paper_needed(x) for x in data])


# 21/10/2021
def part2():
    with open('data.txt') as file:
        data = file.readlines()

    return sum([ribbon_needed(x) for x in data])


def paper_needed(dims):
    s1, s2, s3 = sorted(int(x) for x in dims.split('x'))
    return (2 * s1 * s2) + (2 * s2 * s3) + (2 * s1 * s3) + (s1 * s2)


def ribbon_needed(dims):
    s1, s2, s3 = sorted(int(x) for x in dims.split('x'))
    return (2 * s1) + (2 * s2) + (s1 * s2 * s3)


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
