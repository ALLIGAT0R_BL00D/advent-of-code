from helpers.time import run
import re


def part1():
    time = 2503

    with open('data.txt') as file:
        data = file.readlines()

    furthest = 0
    for deer in data:
        kms, t, rest = re.findall(r'\d+', deer)
        active_dist = int(kms) * int(t)
        active = int(t) + int(rest)

        total_distance = (time // active) * active_dist
        time_remaining = time % active if time % active < int(t) else int(t)

        total_distance += int(kms) * time_remaining

        if total_distance > furthest:
            furthest = total_distance

    return furthest


def part2():
    time = 50

    with open('data.txt') as file:
        data = file.readlines()

    reindeers = {}
    for deer in data:
        name, kms, t, rest = re.findall(r'(^\w+(?= )|\d+)', deer)
        reindeers[name] = {
            'active_distance': int(kms),
            'active_time': int(t),
            'rest_time': int(rest),
            'total_dist': 0,
            'score': 0,
        }

    scores = [0 for _ in reindeers.keys()]
    for i in range(1, time + 1):
        for x, values in reindeers.items():
            print(i, x, values)

        if i == 3:
            return 1

    return 1


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
