import hashlib

from helpers.time import run


# 22/10/2021
def part1():
    data = 'bgvyzdsv'

    i = 0
    while True:
        hex_ = hashlib.md5(f'{data}{i}'.encode()).hexdigest()
        if hex_.startswith('00000'):
            return i
        i += 1


# 22/10/2021
def part2():
    data = 'bgvyzdsv'

    i = 0
    while True:
        hex_ = hashlib.md5(f'{data}{i}'.encode()).hexdigest()
        if hex_.startswith('000000'):
            return i
        i += 1


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
