from helpers.time import run

"""
&	AND         x & y
|	OR          x | y
~	NOT         ~x
^	XOR         x ^ y
>>	right shift	x>>
<<	left shift	x<<
"""
b = 0
signals = {'1': 1}
target = 'a'
n, a, o, ls, rs = 'NOT', 'AND', 'OR', 'LSHIFT', 'RSHIFT'


# 22/10/2021
def part1():
    global b
    with open('data.txt') as file:
        data = file.readlines()

    while True:
        for row in data:
            action, wire = row.split(' -> ')
            wire = wire.strip()

            if wire not in signals.keys() and action.isnumeric():
                signals[wire] = int(action)
            elif action in signals.keys():
                signals[wire] = signals[action]

            if action.startswith(n):
                _, wire_ = action.split(' ')
                if wire_ not in signals.keys():
                    continue
                signals[wire] = 65535 + ~signals[wire_] + 1
            elif a in action:
                wire1, wire2 = action.split(f' {a} ')
                if wire1 not in signals.keys() or wire2 not in signals.keys():
                    continue
                signals[wire] = signals[wire1] & signals[wire2]
            elif o in action:
                wire1, wire2 = action.split(f' {o} ')
                if wire1 not in signals.keys() or wire2 not in signals.keys():
                    continue
                signals[wire] = signals[wire1] | signals[wire2]
            elif rs in action:
                wire_, amount = action.split(f' {rs} ')
                if wire_ not in signals.keys():
                    continue
                signals[wire] = signals[wire_] >> int(amount)
            elif ls in action:
                wire_, amount = action.split(f' {ls} ')
                if wire_ not in signals.keys():
                    continue
                signals[wire] = signals[wire_] << int(amount)

            if target in signals.keys():
                b = signals[target]
                return signals[target]


# 22/10/2021
def part2():
    global signals
    signals = {'1': 1, 'b': b}
    return part1()


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
