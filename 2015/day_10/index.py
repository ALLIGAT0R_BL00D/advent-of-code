from helpers.time import run

times = 40


# 28/10/2021
def part1():
    number = '1321131112'

    for _ in range(times):
        new_number = ''
        last_digit = number[0]
        count = 0
        for digit in number:
            if digit != last_digit:
                new_number += f'{count}{last_digit}'
                count = 1
                last_digit = digit
                continue
            count += 1

        new_number += f'{count}{last_digit}'
        number = new_number

    return len(number)


# 28/10/2021
def part2():
    global times
    times = 50

    return part1()


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
