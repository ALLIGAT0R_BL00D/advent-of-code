from helpers.time import run


def part1():
    with open('data.txt') as file:
        data = file.readline()
        loop = f'{data}{data[0]}'

    return sum(int(digit) for i, digit in enumerate(data)
               if digit == loop[i + 1])


def part2():
    with open('data.txt') as file:
        data = file.readline()

    return sum(int(digit) for i, digit in enumerate(data)
               if digit == data[(i + len(data) // 2) % len(data)])


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
