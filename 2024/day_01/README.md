# Day 1: Historian Hysteria

## [Part 1](https://adventofcode.com/2024/day/1)

I start by creating two lists, one for the left and one for the right. I sort both list ascending. Then I go over both
lists and take the absolute of subtracting one from the other. Sum it all and that's the answer

## [Part 2](https://adventofcode.com/2024/day/1#part2)

I start the same as part one where I create both lists. Then I go over both lists and count the number of times the
number from the left list appears in the right list. That count is multiplied by the number I looked for. All those
products are summed to get the answer 
