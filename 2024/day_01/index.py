from helpers.data import run, multi_line_data, find_all_numbers


def part1():
    data = multi_line_data('data.txt')

    left, right = [], []
    for row in data:
        l, r = find_all_numbers(row)
        left.append(l)
        right.append(r)

    left.sort()
    right.sort()

    return sum(abs(l - r) for l, r in zip(left, right))


def part2():
    data = multi_line_data('data.txt')

    left, right = [], []
    for row in data:
        l, r = find_all_numbers(row)
        left.append(l)
        right.append(r)

    return sum(num * right.count(num) for num in left)


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
