from helpers.data import run, single_line_data, multi_line_data, find_all_positive_numbers, find_all_numbers
from helpers.grid import Grid


###################################################
##################### HELPERS #####################
###################################################

def get_area(garden: Grid, row: int, col: int):
    grid = garden.grid
    char = grid[row][col]
    start = (row, col)

    perimeter = 0
    locations = {start}
    check = {start}
    while len(check) > 0:
        point = check.pop()
        locations.add(point)
        neighbors = garden.get_filtered_neighbors(*point, lambda r, c: grid[r][c] == char)
        perimeter += 4 - len(neighbors)

        check.update([neighbor for neighbor in neighbors if neighbor not in locations])

    return perimeter, locations


###################################################
###################### PARTS ######################
###################################################

def part1(data):
    garden = Grid(data)
    areas = {}

    for r, row in garden.rows:
        for c, col in garden.cols(r):
            if (r, c) not in set().union(*areas.values()):
                perimeter, area = get_area(garden, r, c)
                areas[((r, c), perimeter, len(area))] = area

    return sum(p * a for _, p, a in areas.keys())


def part2(data):
    return -1


if __name__ == '__main__':
    # input_data = single_line_data('data')
    input_data = multi_line_data('data')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
