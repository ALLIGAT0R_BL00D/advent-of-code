import unittest
from unittest import skipIf

from helpers.Puzzle import Puzzle
from .index import part1, part2


class Year2024Day12Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2024, 12)

    def test_p1_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(140, part1(data))

    def test_p1_case_2(self):
        data = self.puzzle.multi_line_practice(2)
        self.assertEqual(772, part1(data))

    def test_p1_case_3(self):
        data = self.puzzle.multi_line_practice(3)
        self.assertEqual(1930, part1(data))

    @skipIf(True, 'take too long')
    def test_p1_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual(1344578, part1(data))

    def test_p2_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(80, part2(data))

    def test_p2_case_2(self):
        data = self.puzzle.multi_line_practice(2)
        self.assertEqual(436, part2(data))

    def test_p2_case_3(self):
        data = self.puzzle.multi_line_practice(4)
        self.assertEqual(236, part2(data))

    def test_p2_case_4(self):
        data = self.puzzle.multi_line_practice(5)
        self.assertEqual(368, part2(data))

    def test_p2_case_5(self):
        data = self.puzzle.multi_line_practice(3)
        self.assertEqual(1206, part1(data))

    def test_p2_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual('solution', part2(data))


if __name__ == '__main__':
    unittest.main()
