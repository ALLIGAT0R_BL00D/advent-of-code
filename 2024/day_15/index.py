import os
from time import sleep

from helpers.Character import Character
from helpers.Direction import Direction
from helpers.data import run, multi_line_data

###################################################
##################### HELPERS #####################
###################################################

USER, WALL, BOX, EMPTY = '@', '#', 'O', '.'
BOX_LEFT, BOX_RIGHT = '[', ']'

direction = {
    '^': Direction.UP,
    '>': Direction.RIGHT,
    'v': Direction.DOWN,
    '<': Direction.LEFT,
}


def clear():
    if os.name == 'nt':
        _ = os.system('cls')
    else:
        _ = os.system('clear')


chars = {
    '#': '🧱',
    '[': '📦',
    ']': '📦',
    '.': '▫️',
}


def visualize(character):
    clear()
    character.grid.show(lambda col, r, c: '🤖' if (r, c) == character.position else chars[col])
    sleep(0.05)


###################################################
###################### PARTS ######################
###################################################

def part1(data):
    one_line = '|'.join(data)
    grid, tasks = one_line.split('||')
    tasks = tasks.replace('|', '')

    character = Character(grid.split('|'))

    start = character.grid.find(USER)
    character.set_start(start)
    character.set_tile(*start, EMPTY)

    for task in tasks:
        visualize(character)
        character.set_direction(direction[task])

        empty_spot = character.look_forward_for(
            find_criteria=lambda r, c: character.get_tile(r, c) == EMPTY,
            stop_criteria=lambda r, c: character.get_tile(r, c) == WALL,
        )

        if empty_spot is None:
            continue

        next_step = character.get_next_step()

        if character.get_tile(*next_step) == EMPTY:
            character.move()
            continue

        character.set_tile(*next_step, EMPTY)
        character.set_tile(*empty_spot, BOX)
        character.move()

    visualize(character)

    total = 0
    for r, row in character.grid.rows:
        for c, col in character.grid.cols(r):
            if col == BOX:
                total += 100 * r + c
    return total


def part2(data):
    one_line = '|'.join(data)
    grid, tasks = one_line.split('||')
    tasks = tasks.replace('|', '')

    expanded_grid = []
    for row in grid.split('|'):
        row = row.replace(WALL, f'{WALL}{WALL}')
        row = row.replace(EMPTY, f'{EMPTY}{EMPTY}')
        row = row.replace(BOX, f'{BOX_LEFT}{BOX_RIGHT}')
        row = row.replace(USER, f'{USER}{EMPTY}')
        expanded_grid.append(row)

    character = Character(expanded_grid)

    start = character.grid.find(USER)
    character.set_start(start)
    character.set_tile(*start, EMPTY)

    for task in tasks:
        visualize(character)
        character.set_direction(direction[task])

        empty_spot = character.look_forward_for(
            find_criteria=lambda r, c: character.get_tile(r, c) == EMPTY,
            stop_criteria=lambda r, c: character.get_tile(r, c) == WALL,
        )

        if empty_spot is None:
            continue

        next_step = character.get_next_step()

        if character.get_tile(*next_step) == EMPTY:
            character.move()
            continue

        if character.direction == Direction.LEFT or character.direction == Direction.RIGHT:
            character.remove_tile(*empty_spot)
            character.add_tile(*next_step, EMPTY)
            character.move()
            continue

        delta_r, _ = character.move_map[character.direction]
        check_boxes = {next_step}
        if character.grid.get_cell(next_step) == BOX_RIGHT:
            check_boxes = {(next_step[0], next_step[1] - 1)}

        boxes_to_move = list(check_boxes)
        can_push = True
        while len(check_boxes) > 0:
            box_r, box_c = check_boxes.pop()

            if (character.grid.get_cell((box_r + delta_r, box_c)) == WALL or
                    character.grid.get_cell((box_r + delta_r, box_c + 1)) == WALL):
                can_push = False
                break

            push_positions = [-1, 0, 1]
            for push_position in push_positions:
                box_to_push_position = (box_r + delta_r, box_c + push_position)
                if character.grid.get_cell(box_to_push_position) == BOX_LEFT:
                    check_boxes.add(box_to_push_position)
                    boxes_to_move.insert(0, box_to_push_position)

        if can_push:
            for box_r, box_c in boxes_to_move:
                character.set_tile(box_r + delta_r, box_c, BOX_LEFT)
                character.set_tile(box_r + delta_r, box_c + 1, BOX_RIGHT)
                character.set_tile(box_r, box_c, EMPTY)
                character.set_tile(box_r, box_c + 1, EMPTY)
            character.move()

    visualize(character)
    total = 0
    for r, row in character.grid.rows:
        for c, col in character.grid.cols(r):
            if col == BOX_LEFT:
                total += 100 * r + c
    return total


if __name__ == '__main__':
    input_data = multi_line_data('data')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
