import unittest

from helpers.Puzzle import Puzzle
from .index import part1, part2


class Year2024Day15Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2024, 15)

    def test_p1_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(2028, part1(data))

    def test_p1_case_2(self):
        data = self.puzzle.multi_line_practice(2)
        self.assertEqual(10092, part1(data))

    def test_p1_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual(1383666, part1(data))

    def test_p2_case_1(self):
        data = self.puzzle.multi_line_practice(2)
        self.assertEqual(9021, part2(data))

    def test_p2_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual(1412866, part2(data))


if __name__ == '__main__':
    unittest.main()
