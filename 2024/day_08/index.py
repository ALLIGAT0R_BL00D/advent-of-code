from helpers.data import run, multi_line_data
from helpers.grid import Grid


###################################################
##################### HELPERS #####################
###################################################

def get_antenna_locations(grid: Grid):
    antenna_locations = {}

    for r, row in grid.rows:
        for c, antenna in grid.cols(r):
            if antenna != '.':
                if antenna not in antenna_locations:
                    antenna_locations[antenna] = []
                antenna_locations[antenna].append((r, c))

    return antenna_locations


###################################################
###################### PARTS ######################
###################################################

def part1(data):
    grid = Grid(data)
    antenna_locations = get_antenna_locations(grid)

    antinodes = set()
    for antenna, locations in antenna_locations.items():
        for i, location in enumerate(locations):
            for j, location2 in enumerate(locations[i + 1:]):
                dr, dc = location2[0] - location[0], location2[1] - location[1]

                antinode1 = location[0] - dr, location[1] - dc
                antinode2 = location2[0] + dr, location2[1] + dc

                if grid.is_in(*antinode1):
                    antinodes.add(antinode1)

                if grid.is_in(*antinode2):
                    antinodes.add(antinode2)

    return len(antinodes)


def part2(data):
    grid = Grid(data)
    antenna_locations = get_antenna_locations(grid)

    antinodes = set()
    for antenna, locations in antenna_locations.items():
        for i, antenna1 in enumerate(locations):
            for antenna2 in locations[i + 1:]:
                dr, dc = antenna2[0] - antenna1[0], antenna2[1] - antenna1[1]

                # create linear function
                start_row = antenna1[0] - (dr / dc) * antenna1[1]
                f = lambda c: ((dr / dc) * c) + start_row

                # get all columns in grid for the function
                left = [x for x in range(antenna1[1], -1, -abs(dc))]
                right = [x for x in range(antenna1[1], grid.cols_len, abs(dc))]
                all_c = set(left + right)
                for col in all_c:
                    antinode = (round(f(col)), col)
                    if grid.is_in(*antinode):
                        antinodes.add(antinode)

    return len(antinodes)


if __name__ == '__main__':
    # input_data = single_line_data('data')
    input_data = multi_line_data('data.txt')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
