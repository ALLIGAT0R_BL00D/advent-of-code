import unittest

from helpers.Puzzle import Puzzle
from .index import part1, part2


class Year2024Day08Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2024, 8)

    def test_p1_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(14, part1(data))

    def test_p1_case_2(self):
        data = self.puzzle.multi_line_practice(2)
        self.assertEqual(2, part1(data))

    def test_p1_case_3(self):
        data = self.puzzle.multi_line_practice(3)
        self.assertEqual(4, part1(data))

    def test_p1_case_4(self):
        data = self.puzzle.multi_line_practice(4)
        self.assertEqual(4, part1(data))

    def test_p1_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual(348, part1(data))

    def test_p2_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(34, part2(data))

    def test_p2_case_2(self):
        data = self.puzzle.multi_line_practice(5)
        self.assertEqual(9, part2(data))

    def test_p2_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual(1221, part2(data))


if __name__ == '__main__':
    unittest.main()
