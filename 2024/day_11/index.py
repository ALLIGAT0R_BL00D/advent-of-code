from helpers.Puzzle import Puzzle
from helpers.data import run, find_all_numbers


###################################################
##################### HELPERS #####################
###################################################


def split_stone(stone):
    stone = str(stone)
    left, right = stone[0:len(stone) // 2], stone[len(stone) // 2:]
    return int(left), int(right)


def add_stone(stones, stone, amount):
    if stone not in stones.keys():
        stones[stone] = 0
    stones[stone] += amount
    return stones


###################################################
###################### PARTS ######################
###################################################

def blink(data, iterations):
    stones = {x: 1 for x in find_all_numbers(data)}
    for _ in range(iterations):
        new_stones = {}
        for stone, amount in stones.items():
            if stone == 0:
                add_stone(new_stones, 1, amount)
            elif len(str(stone)) % 2 == 0:
                stone1, stone2 = split_stone(stone)
                add_stone(new_stones, stone1, amount)
                add_stone(new_stones, stone2, amount)
            else:
                add_stone(new_stones, stone * 2024, amount)

        stones = new_stones
    return sum(stones.values())


if __name__ == '__main__':
    puzzle = Puzzle(2024, 11)
    input_data = puzzle.single_line_data

    print(f'part1 solution: {run(blink, input_data, 25)}')
    print(f'part2 solution: {run(blink, input_data, 75)}')
