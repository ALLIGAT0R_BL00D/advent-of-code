import unittest

from helpers.Puzzle import Puzzle
from .index import blink


class Year2024Day11Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2024, 11)

    def test_p1_case_1(self):
        data = '0 1 10 99 999'
        self.assertEqual(7, blink(data, 1))

    def test_p1_case_2(self):
        data = '125 17'
        self.assertEqual(22, blink(data, 6))

    def test_p1_case_3(self):
        data = '125 17'
        self.assertEqual(55312, blink(data, 25))

    def test_p1_final(self):
        data = self.puzzle.single_line_data
        self.assertEqual(189167, blink(data, 25))

    def test_p2_final(self):
        data = self.puzzle.single_line_data
        self.assertEqual(225253278506288, blink(data, 75))


if __name__ == '__main__':
    unittest.main()
