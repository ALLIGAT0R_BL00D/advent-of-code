# Day 7: Bridge Repair

## [Part 1](https://adventofcode.com/2024/day/7)

I start by parsing the answers and numbers. I have a helper that get all the numbers in a string. With some smart
unpacking, I group the right numbers. Initially I thought of trying all combination in a few for loops. but I got stuck
thinking about it. Then I thought of recursion. You want to do the same task from left to right. My recursive function
takes the answers and numbers. Then my first stop condition is when I'm checking the last numbers. When the sum or
product result in the answer, I return `true` so I can count the answer to a `calibration result`. When there are more
than two numbers left, I rerun this function for both the sum and product. The new call will be with a shorter list of
numbers where the first two are replaced with the sum or product.

## [Part 2](https://adventofcode.com/2024/day/7#part2)

Part two is pretty much the same as part one, but with an extra operator. I copied my recursive function, added the `||`
operation to it, and that was all to part 2. Then I refactored it to be all in one function
