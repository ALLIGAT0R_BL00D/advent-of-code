from helpers.data import run, multi_line_data, find_all_numbers


###################################################
##################### HELPERS #####################
###################################################

def find_answer(answer, numbers, with_or=False):
    x, y = numbers[0], numbers[1]

    if len(numbers) == 2:
        return x * y == answer or x + y == answer or (with_or and int(f'{x}{y}') == answer)

    sum_ok = find_answer(answer, [x + y, *numbers[2::]], with_or)
    product_ok = find_answer(answer, [x * y, *numbers[2::]], with_or)

    if with_or:
        or_ok = find_answer(answer, [int(f'{x}{y}'), *numbers[2::]], with_or)
        return sum_ok or product_ok or or_ok
    return sum_ok or product_ok


###################################################
###################### PARTS ######################
###################################################

def part1(data):
    calibration = 0
    for row in data:
        answer, *numbers = find_all_numbers(row)

        if find_answer(answer, numbers):
            calibration += answer

    return calibration


def part2(data):
    calibration = 0
    for row in data:
        answer, *numbers = find_all_numbers(row)

        if find_answer(answer, numbers, True):
            calibration += answer

    return calibration


if __name__ == '__main__':
    input_data = multi_line_data()

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
