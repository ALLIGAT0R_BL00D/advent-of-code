import unittest

from helpers.Puzzle import Puzzle
from helpers.data import multi_line_data
from .index import part1, part2


class Year2024Day07Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2024, 7)

    def test_p1_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(3749, part1(data))

    def test_p1_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual(2437272016585, part1(data))

    def test_p2_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(11387, part2(data))

    def test_p2_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual(162987117690649, part2(data))


if __name__ == '__main__':
    unittest.main()
