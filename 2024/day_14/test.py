import unittest

from helpers.Puzzle import Puzzle
from .index import part1, part2


class Year2024Day14Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2024, 14)

    def test_p1_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(12, part1(data, 7, 11))

    def test_p1_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual(229421808, part1(data, 103, 101))

    def test_p2_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual(6577, part2(data, self.puzzle))


if __name__ == '__main__':
    unittest.main()
