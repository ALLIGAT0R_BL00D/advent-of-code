from helpers.data import run, multi_line_data, find_all_numbers


def part1(data, rows, cols):
    end = []

    for row in data:
        pc, pr, vc, vr = find_all_numbers(row)

        for i in range(100):
            pr += vr
            pc += vc

        end.append((pr % rows, pc % cols))

    print(len(end))
    total = 1
    total *= len([1 for r, c in end if r < rows // 2 and c < cols // 2])
    total *= len([1 for r, c in end if r < rows // 2 and c > cols // 2])
    total *= len([1 for r, c in end if r > rows // 2 and c < cols // 2])
    total *= len([1 for r, c in end if r > rows // 2 and c > cols // 2])
    return total


def part2(data, puzzle):
    rows, cols = 103, 101
    robots = []

    for row in data:
        pc, pr, vc, vr = find_all_numbers(row)
        robots.append(((pr, pc), (vr, vc)))

    seconds = 0
    while True:
        seconds += 1
        positions = []
        for i, robot in enumerate(robots):
            position, velocity = robot
            pr, pc = position
            vr, vc = velocity

            new_position = ((pr + vr) % rows, (pc + vc) % cols)
            robots[i] = (new_position, velocity)
            positions.append(new_position)

        if seconds == 6577:
            for r in range(rows):
                row = ''
                for c in range(cols):
                    row += '#' if (r, c) in positions else '.'
                puzzle.write(row)
            break

    return seconds


if __name__ == '__main__':
    input_data = multi_line_data('data')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
