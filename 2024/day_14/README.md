# Day 14:

## [Part 1](https://adventofcode.com/2024/day/14)

For step one, I moved all robots based on their velocities and then counted all robots in each quadrant (middle
excluded). Multiplied them and that was the solution.

## [Part 2](https://adventofcode.com/2024/day/14#part2)

For part two, I added the states of all robots to a file for each second until I found a loop. Then I looked through the
file if I could find a tree. I also printed the seconds, So when I found the tree, I looked at the corresponding
seconds.