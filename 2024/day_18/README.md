# Day 18: RAM Run

## [Part 1](https://adventofcode.com/2024/day/18)

This is just astar, nothing special

## [Part 2](https://adventofcode.com/2024/day/18#part2)

In part two, you have to look for the first braking `byte`. I do this with a binary search. I have a pointer in the
middle of the coordinates I need to check. When I create a grid with all those and don't find a path, something before
broke it. So I move the pointer halfway to the front. And the same for when I do find a path, but then I move it
forward, as I can have more `bytes` in my grid. I repeat that, until the pointer doesn't move. That is the breaking
`byte`
