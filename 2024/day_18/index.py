from helpers.data import run, multi_line_data, find_all_numbers
from helpers.grid import Grid


###################################################
##################### HELPERS #####################
###################################################

def get_h(end):
    return lambda point: abs(end[0] - point[0]) + abs(end[1] - point[1])


def reconstruct_path(came_from, current):
    path = [current]
    while current in came_from.keys():
        current = came_from[current]
        path.insert(0, current)
    return path


def astar(grid, start, end, h):
    open_set = {start}
    came_from = {}

    g_score = {start: 0}
    f_score = {start: h(start)}

    while len(open_set) > 0:
        current = min({k: v for k, v in f_score.items() if k in open_set}, key=f_score.get)
        if current == end:
            return reconstruct_path(came_from, current)

        open_set.remove(current)
        for neighbor in grid.get_filtered_neighbors(*current, lambda r, c: grid.get_cell((r, c)) != '#'):
            tentative_g_score = g_score[current] + 1
            if neighbor not in g_score or tentative_g_score < g_score[neighbor]:
                came_from[neighbor] = current
                g_score[neighbor] = tentative_g_score
                f_score[neighbor] = tentative_g_score + h(neighbor)

                if neighbor not in open_set:
                    open_set.add(neighbor)
    return None


def get_pointer(min_pointer, max_pointer):
    return min_pointer + ((max_pointer - min_pointer) // 2)


###################################################
###################### PARTS ######################
###################################################

def part1(data, b):
    coordinates = [tuple(find_all_numbers(row)[::-1]) for row in data]
    max_row = max(coordinates, key=lambda x: x[1])[1]
    max_col = max(coordinates, key=lambda x: x[0])[0]
    grid = [['#' if (r, c) in coordinates[0:b] else '.' for c in range(max_col + 1)] for r in range(max_row + 1)]

    grid = Grid(grid)
    start = (0, 0)
    end = (max_row, max_col)

    path = astar(grid, start, end, get_h(end))

    return len(path) - 1


def part2(data, b):
    coordinates = [tuple(find_all_numbers(row)[::-1]) for row in data]
    max_row = max(coordinates, key=lambda x: x[1])[1]
    max_col = max(coordinates, key=lambda x: x[0])[0]
    start = (0, 0)
    end = (max_row, max_col)

    min_pointer = b
    max_pointer = len(coordinates)
    while True:
        pointer = get_pointer(min_pointer, max_pointer)

        if pointer == min_pointer:
            return f'{coordinates[pointer][1]}, {coordinates[pointer][0]}'

        grid = [['#' if (r, c) in coordinates[0:pointer] else '.' for c in range(max_col + 1)] for r in
                range(max_row + 1)]
        grid = Grid(grid)
        path = astar(grid, start, end, get_h(end))

        if path is None:
            max_pointer = get_pointer(min_pointer, max_pointer)
        else:
            min_pointer = get_pointer(min_pointer, max_pointer)


if __name__ == '__main__':
    # input_data = single_line_data('data')
    input_data = multi_line_data('data')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
