import unittest

from helpers.Puzzle import Puzzle
from .index import part1, part2


class Year2024Day18Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2024, 18)

    def test_p1_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(22, part1(data, 12))

    def test_p1_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual(372, part1(data, 1024))

    def test_p2_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual('6, 1', part2(data, 12))

    def test_p2_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual('25, 6', part2(data, 1024))


if __name__ == '__main__':
    unittest.main()
