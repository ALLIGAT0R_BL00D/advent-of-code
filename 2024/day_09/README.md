# Day 9: Disk Fragmenter

## [Part 1](https://adventofcode.com/2024/day/9)

I start by creating the `files` and `free space`. I go over the list of numbers. Every even numbers (index) will be for
the `files`, and the odd numbers (index) will be `free space`. I add the number of `files` and `free space`
respectively. I made this a helper, because I also need it for part 2.

Then I go over the full list of `files` and `free space`. Whenever I find free space, I look for the last file to move
there. I do this by keeping an index that starts from the ends and goes backwards. After that it's a matter of summing
all products of the `files` and their indexes

## [Part 2](https://adventofcode.com/2024/day/9#part2)

I start by getting the `files` and `free space` with my helper. Then I have a while loop that runs until it doesn't have
`free space` to claim anymore. in each iteration, I start by getting the next group of files to try to move, starting
from the end. When no free space is found for the group of `files`, it gets ignored. Then I move my index pointer and
it's ready to try to move the next file group. At the end, I get the checksum just like part one.
