import unittest

from helpers.Puzzle import Puzzle
from .index import part1, part2


class YearDayTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2024, 9)

    def test_p1_case_1(self):
        data = '2333133121414131402'
        self.assertEqual(1928, part1(data))

    def test_p1_final(self):
        data = self.puzzle.single_line_data
        self.assertEqual(6399153661894, part1(data))

    def test_p2_case_1(self):
        data = '2333133121414131402'
        self.assertEqual(2858, part2(data))

    def test_p2_final(self):
        data = self.puzzle.single_line_data
        self.assertEqual(6421724645083, part2(data))


if __name__ == '__main__':
    unittest.main()
