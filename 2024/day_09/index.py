from helpers.data import run, single_line_data

###################################################
##################### HELPERS #####################
###################################################

free = '.'


def get_blocks(string):
    blocks, n = [], 0
    for i, x in enumerate(string):
        num = int(x)

        if i % 2 == 0:
            for y in range(num):
                blocks.append(str(n))
            n += 1
        else:
            for y in range(num):
                blocks.append('.')

    return blocks


def get_next_group(disk, end):
    temp, group = end, []

    while disk[temp] == free:
        temp -= 1

    end_start, digit = temp, disk[temp]
    while temp > -len(disk) and disk[temp] == digit:
        group.append(digit)
        temp -= 1
    return group, end_start


def fill_next_free_space(disk, file_group, end):
    start, empty_block_length = 0, 0
    search_until = disk.index(file_group[0])

    found_empty_slot = False
    for i in range(search_until):
        if empty_block_length >= len(file_group):
            found_empty_slot = True
            break
        if disk[i] != free:
            continue

        temp, empty_block_length = i, 0
        while temp < len(disk) and disk[temp] == free:
            empty_block_length += 1
            temp += 1
        start = i

    if found_empty_slot:
        for i, x in enumerate(file_group):
            disk[start + i] = file_group[0]
            disk[end - i] = free

    return disk


def get_checksum(disk):
    return sum(i * int(x) for i, x in enumerate(disk) if x != free)


###################################################
###################### PARTS ######################
###################################################

def part1(data):
    disk = get_blocks(data)

    end = -1
    for i, slot in enumerate(disk):
        if slot == free:
            # find last file to move
            while disk[end] == free:
                end -= 1

            disk[i] = disk[end]
            disk[end] = free
            end -= 1

        if i == len(disk) + end:
            break

    return get_checksum(disk)


def part2(data):
    disk = get_blocks(data)

    end = -1
    while disk[0:end].count(free) > 0:
        file_group, end_start = get_next_group(disk, end)
        disk = fill_next_free_space(disk, file_group, end_start)
        end = end_start - len(file_group)

    return get_checksum(disk)


if __name__ == '__main__':
    input_data = single_line_data('data')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
