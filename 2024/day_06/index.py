from dis import Positions

from helpers.data import run, single_line_data, multi_line_data, find_all_positive_numbers, find_all_numbers
from enum import Enum


###################################################
##################### HELPERS #####################
###################################################

class Direction(Enum):
    UP = 'up'
    RIGHT = 'right'
    DOWN = 'down'
    LEFT = 'left'


def find_start_position(grid):
    for y, row in enumerate(grid):
        for x, col in enumerate(row):
            if col == '^':
                return x, y
    return None


def turn_right(direction):
    return {
        Direction.UP: Direction.RIGHT,
        Direction.RIGHT: Direction.DOWN,
        Direction.DOWN: Direction.LEFT,
        Direction.LEFT: Direction.UP,
    }[direction]


def move(direction):
    return {
        Direction.UP: (0, -1),
        Direction.RIGHT: (1, 0),
        Direction.DOWN: (0, 1),
        Direction.LEFT: (-1, 0),
    }[direction]


def get_new_position(position, direction):
    move_x, move_y = move(direction)
    pos_x, pos_y = position
    return pos_x + move_x, pos_y + move_y


def can_make_move(grid, position, direction):
    new_x, new_y = get_new_position(position, direction)

    # out of bound
    if new_x < 0 or new_x >= len(grid[0]):
        return None
    if new_y < 0 or new_y >= len(grid):
        return None

    if grid[new_y][new_x] == '#':
        return False
    return True


def can_create_loop(grid, visited, position, direction):
    new_direction = turn_right(direction)
    # print(new_direction)

    # already visited path
    if new_direction in visited[position]:
        # print(position, visited[position])
        return True

    # disconnected path that would lead in a loop
    new_position = position
    while can_make_move(grid, new_position, new_direction):
        new_position = get_new_position(new_position, new_direction)
        # print(new_position)

        if new_position not in visited.keys():
            continue

        if new_direction in visited[new_position]:
            return True

    return False


###################################################
###################### PARTS ######################
###################################################

def part1():
    data = multi_line_data('data.txt')
    grid = [[col for col in row] for row in data]
    position = find_start_position(grid)
    visited = {position}
    direction = Direction.UP

    while can_make_move(grid, position, direction) is not None:
        if can_make_move(grid, position, direction):
            position = get_new_position(position, direction)
            visited.add(position)
        else:
            direction = turn_right(direction)

    return len(visited)


def print_grid(grid, visited, block):
    for y, row in enumerate(grid):
        print_row = ''
        for x, col in enumerate(row):

            if (x, y) in visited.keys():
                print_row += 'X'
            elif (x, y) == block:
                print_row += 'O'
            else:
                print_row += col
        print(print_row)
    print()


# 529 low
# 897 low
# 904 low
# 2410
def part2():
    # data = single_line_data('practice')
    data = multi_line_data('data.txt')
    grid = [[col for col in row] for row in data]
    start_position = find_start_position(grid)
    position = start_position
    visited = {position: {Direction.UP}}
    direction = Direction.UP

    while can_make_move(grid, position, direction) is not None:
        if can_make_move(grid, position, direction):
            position = get_new_position(position, direction)

            if position not in visited.keys():
                visited[position] = set()
            visited[position].add(direction)
        else:
            direction = turn_right(direction)

    position = start_position
    direction = Direction.UP
    block_count = 0
    blocks = set()
    while can_make_move(grid, position, direction) is not None:
        if can_make_move(grid, position, direction):
            if can_create_loop(grid, visited, position, direction):
                # print(get_new_position(position, direction))
                block_count += 1
                blocks.add(get_new_position(position, direction))

            position = get_new_position(position, direction)
        else:
            direction = turn_right(direction)

    print(len(blocks), block_count)
    return len(blocks)


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
