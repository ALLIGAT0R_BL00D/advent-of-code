# Day 5: Print Queue

## [Part 1](https://adventofcode.com/2024/day/5)

For part 1, I sum all the middle number of correct pages. Which was exactly the tasks... I get the correct pages by
going over each `update`. For each update I check both the left and right side of it whether it satisfies the rules. As
soon as I find an `update` data doesn't satisfy the rule, the whole page it wrong.

## [Part 2](https://adventofcode.com/2024/day/5#part2)

Similar to [day 2](https://gitlab.com/lorenzokorn/advent-of-code/-/blob/master/2024/day_02/README.md?ref_type=heads), I
get all the incorrect pages. To make these correct, I go over each pair and swap the number based on the rules when they
don't satisfy it. As there can be many mistakes on a page, I repeat this process until the page is correct. 

```
First round
97 13 75 29 47  good
97 75 13 29 47  wrong -> rule: 75|13
97 75 29 13 47  wrong -> rule: 29|13
97 75 29 47 13  wrong -> rule: 47|13

Second round:
97 75 29 47 13  good
97 75 29 47 13  good
97 75 47 29 13  wrong -> rule: 47|29
97 75 47 29 13  good

Now it's correct and 47 will be added to the total
```

Then I take the number in the middle and add it to the total. 
