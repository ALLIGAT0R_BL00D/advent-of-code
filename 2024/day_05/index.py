from helpers.data import run, multi_line_data, find_all_numbers


###################################################
##################### HELPERS #####################
###################################################


def get_rules_n_pages():
    data = multi_line_data('data.txt')

    apply_rules = True
    rules, pages = [], []
    for row in data:
        if row == '':
            apply_rules = False
            continue

        if apply_rules:
            rules.append(find_all_numbers(row))
        else:
            pages.append(find_all_numbers(row))

    return rules, pages


def is_left_good(array, update, rules):
    for u in array:
        if [u, update] not in rules:
            return False
    return True


def is_right_good(array, update, rules):
    for u in array:
        if [update, u] not in rules:
            return False
    return True


def is_page_correct(page, rules):
    for i, update in enumerate(page):
        left, right = page[0:i], page[i + 1: len(page)]

        if not is_left_good(left, update, rules) or not is_right_good(right, update, rules):
            return False

    return True


def get_incorrect_pages(pages, rules):
    incorrect_pages = []
    for page in pages:
        if not is_page_correct(page, rules):
            incorrect_pages.append(page)

    return incorrect_pages


def get_middle(array):
    return array[(len(array) - 1) // 2]


###################################################
###################### PARTS ######################
###################################################

def part1():
    rules, pages = get_rules_n_pages()
    return sum(get_middle(page) for page in pages if is_page_correct(page, rules))


def part2():
    rules, pages = get_rules_n_pages()
    incorrect_pages = get_incorrect_pages(pages, rules)

    total = 0
    for page in incorrect_pages:
        while not is_page_correct(page, rules):
            for i in range(len(page) - 1):
                update = page[i]
                next_update = page[i + 1]

                if [update, next_update] not in rules:
                    page[i] = next_update
                    page[i + 1] = update

        total += get_middle(page)
    return total


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
