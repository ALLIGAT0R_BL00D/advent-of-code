import re

from helpers.data import run, multi_line_data, find_all_numbers, shift_right, xor


###################################################
##################### HELPERS #####################
###################################################

def run_program(a, b, c, instructions):
    pointer, output = 0, []

    while pointer < len(instructions) - 1:
        opcode, operand = instructions[pointer], instructions[pointer + 1]
        combo_operantis = operand if operand <= 3 else {4: a, 5: b, 6: c, 7: None}[operand]

        match opcode:
            case 0:
                a = shift_right(a, combo_operantis)
            case 1:
                b = xor(b, operand)
            case 2:
                b = combo_operantis % 8
            case 3 if a > 0:
                if pointer != operand:
                    pointer = operand
                    continue
            case 4:
                b = xor(b, c)
            case 5:
                output.append(combo_operantis % 8)
            case 6:
                b = shift_right(a, combo_operantis)
            case 7:
                c = shift_right(a, combo_operantis)

        pointer += 2

    return output


###################################################
###################### PARTS ######################
###################################################

def part1(data):
    data = ''.join(data)
    a, b, c, *instructions = find_all_numbers(data)
    return ','.join(str(x) for x in run_program(a, b, c, instructions))


def part2(data):
    data = ''.join(data)
    a, b, c, *instructions = find_all_numbers(data)

    todos = [(1, 0)]
    for i, try_a in todos:

        # try the next 8 values for a
        for a in range(try_a, try_a + 8):

            # The output matches the last part of the instructions
            # Since this is based on the todos, this will only loop len(instructions) times
            if run_program(a, 0, 0, instructions) == instructions[-i:]:

                # idk why *8
                todos.append((i + 1, a * 8))
                if i == len(instructions):
                    return a


if __name__ == '__main__':
    # input_data = single_line_data('data')
    input_data = multi_line_data('data')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
