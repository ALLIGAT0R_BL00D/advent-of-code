import unittest

from helpers.Puzzle import Puzzle
from .index import part1, part2


class Year2024Day17Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2024, 17)

    def test_p1_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual('4,6,3,5,6,3,5,2,1,0', part1(data))

    def test_p1_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual('1,5,0,1,7,4,1,0,3', part1(data))

    def test_p2_case_1(self):
        data = self.puzzle.multi_line_practice(2)
        self.assertEqual(117440, part2(data))

    def test_p2_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual(47910079998866, part2(data))


if __name__ == '__main__':
    unittest.main()
