# Day 17:

## [Part 1](https://adventofcode.com/2024/day/17)

In part one, it was literally following all the instructions.

I did learn some things:

```math
a / 2^b = a >> b
```

Bit shifting `a` `b` times to the right, divides `a` by `2^b` and results in an integer.

`xor` is represented by `^` in code which does it on binary level

```math
29 ^ 7 = 9
```

because:

```math
11101 = 29
00111 = 7
----------- xor
11010 = 26
```

Module 8 is the same as an `and` with 7. Also because of the binary level

```math
a % 8 = a & 7

11101 = 29
00111 = 7
----------- and
00101 = 5 (29 / 8 = 24, with 5 remaining)
```

## [Part 2](https://adventofcode.com/2024/day/17#part2)

My bruteforce worked, but it would take ages, so I style an optimalization. I'm still figuring out why the `* 8`, I
don't get it
