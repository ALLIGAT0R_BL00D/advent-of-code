import math

from tqdm import tqdm

from helpers.data import run, single_line_data, multi_line_data, find_all_positive_numbers, find_all_numbers


###################################################
##################### HELPERS #####################
###################################################

def find_prize(button_a, button_b, prize, min_plays=1, max_plays=999_999_999_999):
    (ax, ay), (bx, by) = button_a, button_b

    x, y = prize

    b_plays = 0
    while x > 0 and y > 0:
        b_plays += 1
        x -= bx
        y -= by

        a_plays = x // ax
        if a_plays * ax == x and a_plays * ay == y:
            return 3 * (x / ax) + b_plays

    return 0


###################################################
###################### PARTS ######################
###################################################

def part1(data):
    data = '|'.join(data).split('||')

    games = []

    for row in data:
        ax, ay, bx, by, px, py = find_all_numbers(row)
        games.append(((ax, ay), (bx, by), (px, py)))

    total = 0
    for a, b, p in games:
        total += find_prize(a, b, p, max_plays=100)

    return total


def part2(data):
    data = '|'.join(data).split('||')
    correction = 10000000000000

    games = []

    for row in data:
        ax, ay, bx, by, px, py = find_all_numbers(row)
        games.append(((ax, ay), (bx, by), (px + correction, py + correction)))

    total = 0
    for a, b, p in tqdm(games):
        total += find_prize(a, b, p, min_plays=1_000_000)

    return total


if __name__ == '__main__':
    # input_data = single_line_data('data')
    input_data = multi_line_data('data')

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
