import unittest
from unittest import skipIf

from helpers.Puzzle import Puzzle
from .index import part1, part2


class Year2024Day13Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.puzzle = Puzzle(2024, 13)

    def test_p1_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual(480, part1(data))

    def test_p1_final(self):
        data = self.puzzle.multi_line_data
        self.assertEqual(29438, part1(data))

    def test_p2_case_1(self):
        data = self.puzzle.multi_line_practice()
        self.assertEqual('idk', part2(data))

    # def test_p2_final(self):
    #     data = self.puzzle.multi_line_data
    #     self.assertEqual('solution', part2(data))


if __name__ == '__main__':
    unittest.main()
