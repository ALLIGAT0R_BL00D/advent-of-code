from helpers.Puzzle import Puzzle
from helpers.data import run, multi_line_data
from helpers.grid import Grid


###################################################
##################### HELPERS #####################
###################################################

def get_trailheads(grid):
    starts = []

    for r, row in grid.rows:
        for c, col in grid.cols(r):
            if col != '.' and int(col) == 0:
                starts.append((r, c))

    return starts


def is_increased_step(grid, start, end):
    if grid.get_cell(start) == '.' or grid.get_cell(end) == '.':
        return False
    return int(grid.get_cell(start)) + 1 == int(grid.get_cell(end))


###################################################
###################### PARTS ######################
###################################################

def part1(data):
    grid = Grid(data)
    trail_heads = get_trailheads(grid)
    score = 0

    for trail_head_r, trail_head_c in trail_heads:
        positions = {(trail_head_r, trail_head_c)}

        for step in range(1, 10):
            new_positions = set()
            for r, c in positions:
                new_positions.update(grid.get_filtered_neighbors(
                    r,
                    c,
                    lambda rr, cc: is_increased_step(grid, (r, c), (rr, cc))
                ))
            positions = new_positions

        score += len(positions)

    return score


def part2(data):
    grid = Grid(data)
    trail_heads = get_trailheads(grid)
    score = 0

    for trail_head_r, trail_head_c in trail_heads:
        positions = [(trail_head_r, trail_head_c)]

        for step in range(1, 10):
            new_positions = []
            for r, c in positions:
                new_positions.extend(grid.get_filtered_neighbors(
                    r,
                    c,
                    lambda rr, cc: is_increased_step(grid, (r, c), (rr, cc))
                ))
            positions = new_positions

        score += len(positions)

    return score


if __name__ == '__main__':
    puzzle = Puzzle(2024, 10)
    input_data = puzzle.multi_line_data

    print(f'part1 solution: {run(part1, input_data)}')
    print(f'part2 solution: {run(part2, input_data)}')
