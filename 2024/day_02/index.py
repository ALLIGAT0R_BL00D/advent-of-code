from helpers.data import run, multi_line_data, find_all_numbers


###################################################
##################### HELPERS #####################
###################################################

def get_unsafe_reports(reports):
    """
    Collect all unsafe reports
    """
    unsafe_reports = []
    for report in reports:
        if is_unsafe_report(report):
            unsafe_reports.append(report)

    return unsafe_reports


def is_unsafe_report(report):
    """
    Check whether the report is unsafe based on all reported levels
    """
    is_increasing = False

    for i, level in enumerate(report):
        if i == 0:
            is_increasing = level - report[i + 1] > 0
            continue

        diff = report[i - 1] - level
        if is_unsafe(diff, is_increasing):
            return True
    return False


def is_unsafe(diff, is_increasing):
    """
    Check whether a levels change is safe. This is the case when:
    * the levels are not the same
    * the levels are in the same direction (asc, desc)
    * the levels' change is 1, 2, or 3
    """
    return (diff == 0) or (is_increasing and diff < 0) or (not is_increasing and diff > 0) or (abs(diff) > 3)


###################################################
###################### PARTS ######################
###################################################

def part1():
    data = multi_line_data('data.txt')
    reports = [find_all_numbers(row) for row in data]
    unsafe_reports = get_unsafe_reports(reports)
    return len(reports) - len(unsafe_reports)


def part2():
    data = multi_line_data('data.txt')
    reports = [find_all_numbers(row) for row in data]
    unsafe_reports = get_unsafe_reports(reports)

    total = 0
    for unsafe_report in unsafe_reports:
        no_fix = 0
        for i, num in enumerate(unsafe_report):
            temp_report = unsafe_report[0:i] + unsafe_report[i + 1::]
            no_fix += is_unsafe_report(temp_report)

        # Removing each number did not fix the report
        total += no_fix != len(unsafe_report)

    return len(reports) - len(unsafe_reports) + total


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
