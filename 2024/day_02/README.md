# Day 2: Red-Nosed Reports

## [Part 1](https://adventofcode.com/2024/day/2)

I created a helper to get all the unsafe reports (important for part 2!). What I do here is going over every report and
check whether the level changes are safe. Meaning that they are withing a certain range and all in the same direction.
When I have all the unsafe reports, getting the safe ones is very easy. That's just `all reports - unsafe reports`

## [Part 2](https://adventofcode.com/2024/day/2#part2)

First I get all the unsafe reports as those are the ones we want to try to fix. Then for every report I removed each
level to see if that would fix it. After checking all levels in the report, I got a score of how many levels wouldn't
fix the report. when this number was the same as the length of the report, I know that there are more than one error in
the report, and it's still unsafe. So we don't count those. At the end I end up with the number of fixed reports. To get
the answer, I have to add the already safe reports.
