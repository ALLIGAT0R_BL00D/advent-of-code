from helpers.data import run, multi_line_data


###################################################
##################### HELPERS #####################
###################################################

def is_guess_correct(guess):
    return guess == 'XMAS'


def finishes_mas(letter):
    return letter == 'M' or letter == 'S'


###################################################
###################### PARTS ######################
###################################################

def part1():
    data = multi_line_data('data.txt')
    buffer = 3
    total = 0

    for r in range(len(data)):
        for c in range(len(data[r])):
            if data[r][c] != 'X':
                continue

            no_top = r - buffer < 0
            no_right = c + buffer >= len(data[r])
            no_bottom = r + buffer >= len(data)
            no_left = c - buffer < 0

            # top left
            if not no_top and not no_left:
                guess = data[r][c] + data[r - 1][c - 1] + data[r - 2][c - 2] + data[r - 3][c - 3]
                total += is_guess_correct(guess)

            # top
            if not no_top:
                guess = data[r][c] + data[r - 1][c] + data[r - 2][c] + data[r - 3][c]
                total += is_guess_correct(guess)

            # top right
            if not no_top and not no_right:
                guess = data[r][c] + data[r - 1][c + 1] + data[r - 2][c + 2] + data[r - 3][c + 3]
                total += is_guess_correct(guess)

            # left
            if not no_left:
                guess = data[r][c] + data[r][c - 1] + data[r][c - 2] + data[r][c - 3]
                total += is_guess_correct(guess)

            # right
            if not no_right:
                guess = data[r][c] + data[r][c + 1] + data[r][c + 2] + data[r][c + 3]
                total += is_guess_correct(guess)

            # bottom left
            if not no_bottom and not no_left:
                guess = data[r][c] + data[r + 1][c - 1] + data[r + 2][c - 2] + data[r + 3][c - 3]
                total += is_guess_correct(guess)

            # bottom
            if not no_bottom:
                guess = data[r][c] + data[r + 1][c] + data[r + 2][c] + data[r + 3][c]
                total += is_guess_correct(guess)

            # bottom right
            if not no_bottom and not no_right:
                guess = data[r][c] + data[r + 1][c + 1] + data[r + 2][c + 2] + data[r + 3][c + 3]
                total += is_guess_correct(guess)
    return total


def part2():
    data = multi_line_data('data.txt')
    total = 0

    for r in range(1, len(data) - 1):
        for c in range(1, len(data[r]) - 1):
            if data[r][c] != 'A':
                continue

            # top left & bottom right (\)
            top_left = data[r - 1][c - 1]
            bottom_right = data[r + 1][c + 1]
            tlbr = finishes_mas(top_left) and finishes_mas(bottom_right) and top_left != bottom_right

            # bottom left & top right (/)
            bottom_left = data[r + 1][c - 1]
            top_right = data[r - 1][c + 1]
            bltr = finishes_mas(bottom_left) and finishes_mas(top_right) and bottom_left != top_right

            total += tlbr and bltr
    return total


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
