# Day 4: Ceres Search

## [Part 1](https://adventofcode.com/2024/day/4)

This one is better explained visually. Basically, I go over all letters and for every `X` look around it for `XMAS`

```
╔═══╗          ╔═══╗          ╔═══╗
║ s ║          ║ s ║          ║ s ║
╚═══╝          ╚═══╝          ╚═══╝
     ╔═══╗     ╔═══╗     ╔═══╗
     ║ A ║     ║ A ║     ║ A ║
     ╚═══╝     ╚═══╝     ╚═══╝
          ╔═══╗╔═══╗╔═══╗
          ║ M ║║ M ║║ M ║
          ╚═══╝╚═══╝╚═══╝
╔═══╗╔═══╗╔═══╗╔═══╗╔═══╗╔═══╗╔═══╗
║ s ║║ A ║║ M ║║ X ║║ M ║║ A ║║ s ║
╚═══╝╚═══╝╚═══╝╚═══╝╚═══╝╚═══╝╚═══╝
          ╔═══╗╔═══╗╔═══╗
          ║ M ║║ M ║║ M ║
          ╚═══╝╚═══╝╚═══╝
     ╔═══╗     ╔═══╗     ╔═══╗
     ║ A ║     ║ A ║     ║ A ║
     ╚═══╝     ╚═══╝     ╚═══╝
╔═══╗          ╔═══╗          ╔═══╗
║ s ║          ║ s ║          ║ s ║
╚═══╝          ╚═══╝          ╚═══╝
```

I had to think about the edges, because you can't look for indexes outside the grid. So I check whether a direction
wouldn't result in `XMAS` without even checking it.

```

     ╔══════════════════════════════════════════════
     ║      \    |    /
     ║      
     ║         ╔═══╗╔═══╗╔═══╗╔═══╗
     ║ -    -  ║ X ║║ M ║║ A ║║ s ║
     ║         ╚═══╝╚═══╝╚═══╝╚═══╝
     ║         ╔═══╗╔═══╗
     ║      /  ║ M ║║ M ║
     ║         ╚═══╝╚═══╝
     ║         ╔═══╗     ╔═══╗
     ║ /       ║ A ║     ║ A ║
     ║         ╚═══╝     ╚═══╝
     ║         ╔═══╗          ╔═══╗
     ║         ║ s ║          ║ s ║
     ║         ╚═══╝          ╚═══╝
     ║
     ║
     ║
     ║
```

As I know the coordinates of all letters relative to the `X`, I run this algorithm in `0(n^2)`

## [Part 2](https://adventofcode.com/2024/day/4#part2)

Part 2 is very similar, but a simpler. instead of looking for `XMAS` in 8 directions, we look for `MAS` in the two
diagonals. A little optimization is that I ignore the full outer ring. Thos can't have the `X-MAS`, because it will be
part out of bound. Same as before, I go over all the remaining letters and for every `A`, I check if the diagonals are
an `M` and an `S`, but not the same! Otherwise, you'd get `MOM` or `SAS`. With that simple check, I find all the `X-MAS`
going over the grid once with a total time complexity of `O(n^2)`. Which is only because I have to go though the grid.
