import re

from helpers.data import run, multi_line_data


def part1():
    data = multi_line_data('data.txt')

    total = 0
    for memory in data:
        finds = re.findall(r'(?:mul\((\d+),(\d+)\))', memory)
        total += sum(int(x) * int(y) for x, y in finds)
    return total


def part2():
    data = multi_line_data('data.txt')

    total = 0
    enabled = True  # Important note: lines don't stop the sequence
    for memory in data:
        finds = re.findall(r'(?:mul\((\d+),(\d+)\))|(do\(\)|don\'t\(\))', memory)

        for (x, y, instruction) in finds:
            if enabled and instruction == '':
                total += int(x) * int(y)
            else:
                if instruction == "do()":
                    enabled = True
                else:
                    enabled = False

    return total


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
