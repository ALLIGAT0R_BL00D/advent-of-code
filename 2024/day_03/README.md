# Day 3: Mull It Over

## [Part 1](https://adventofcode.com/2024/day/3)

We have to find all `mul(x,y)` in the given strings and multiply `x` and `y`. This is easy work with some simple regex:

```regexp
(?:mul\((\d+),(\d+)\))
```

This is a `positive lookup` for the `mul(x,y)`, but with the added bonus that I get `x` and `y` as groups in the results
directly. So I multiply all combinations I find and sum those.

## [Part 2](https://adventofcode.com/2024/day/3#part2)

Part 2 is similar to part one, but the regex is extended. Now I also look for `do()` and `don't()` so that I enable and
disable multiplying my findings.

```regexp
(?:mul\((\d+),(\d+)\))|(do\(\)|don\'t\(\))
```

Something I didn't come up with myself is that I had to continue from the last sequence. So my code didn't work while it
was correct. I watch a [video](https://www.youtube.com/watch?v=83Svq4UB8f4) where the assignment was explained. and only
when I looked at the code and noticed this guy making one line of the input is when I realized the line:

> Only the most recent `do()` or `don't()` instruction applies.

My program reset to be enabled for every sequence while it shouldn't have when the previous sequence ended disabled. 
