def part1():
    with open('data.txt') as file:
        calories = [x.strip() for x in file.readlines()]

    elve_calories = {}

    i = 0
    for c in calories:
        if c == '':
            i = i + 1
            continue

        if i not in elve_calories:
            elve_calories[i] = 0

        elve_calories[i] = elve_calories[i] + int(c)

    return max(elve_calories.values())


def part2():
    with open('data.txt') as file:
        calories = [x.strip() for x in file.readlines()]

    elve_calories = {}

    i = 0
    for c in calories:
        if c == '':
            i = i + 1
            continue

        if i not in elve_calories:
            elve_calories[i] = 0

        elve_calories[i] = elve_calories[i] + int(c)

    return sum(sorted(elve_calories.values())[-3::])


if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution: {part2()}')
