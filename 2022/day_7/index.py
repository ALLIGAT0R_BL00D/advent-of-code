import re


def part1():
    min_size = 100_000

    with open('data.txt') as file:
        commands = [line.strip() for line in file.readlines()]

    cd, folder = ['$ cd', 'dir']
    directories = []
    directories_sizes = {}

    for command in commands:
        if cd in command:
            directory = command.split(' ')[2]  # 0 is $, 1 is cd

            if directory == '/':
                directories = []
            elif directory == '..':
                directories = directories[0:-1]
            else:
                # add path to the directory to the directories array
                directories.append('-'.join([*directories, directory]))
                directories_sizes[directories[-1]] = 0
        elif re.match(r'\d+', command):
            file_size = int(command.split(' ')[0])

            # add the file size to the directories and all the parent directories
            for i, path in enumerate(directories):
                directories_sizes[path] = directories_sizes[path] + file_size

    return sum(x for x in directories_sizes.values() if x <= min_size)


def part2():
    max_space = 70_000_000
    min_space = 30_000_000

    with open('data.txt') as file:
        commands = [line.strip() for line in file.readlines()]

    cd, folder = ['$ cd', 'dir']
    directories = []
    directories_sizes = {}

    for command in commands:
        if cd in command:
            directory = command.split(' ')[2]  # 0 is $, 1 is cd

            if directory == '..':
                directories = directories[0:-1]
            else:
                # add path to the directory to the directories array
                directories.append('-'.join([*directories, directory]))
                directories_sizes[directories[-1]] = 0
        elif re.match(r'\d+', command):
            file_size = int(command.split(' ')[0])

            for i, path in enumerate(directories):
                # add the file size to the directories and all the parent directories
                directories_sizes[path] = directories_sizes[path] + file_size

    unused_space = max_space - directories_sizes['/']
    required_space = min_space - unused_space
    return min(x for x in directories_sizes.values() if x > required_space)


if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution: {part2()}')
