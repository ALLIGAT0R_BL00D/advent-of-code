trees_found = []


def part1():
    global trees_found
    with open('data.txt') as file:
        rows = [line.strip() for line in file.readlines()]

    for row_i, row in enumerate(rows):
        for i, tree in enumerate(row):
            if i == 0 or i == len(row) - 1:
                trees_found.append((row_i, i))
                continue

            # right
            if tree > max(row[i + 1::]):
                trees_found.append((row_i, i))
                continue

            # left
            if tree > max(row[0:i]):
                trees_found.append((row_i, i))

    for column_i, column in enumerate([*zip(*rows)]):
        for i, tree in enumerate(column):
            if (i, column_i) in trees_found:
                continue
            if i == 0 or i == len(column) - 1:
                trees_found.append((i, column_i))
                continue

            # below
            if tree > max(column[i + 1::]):
                trees_found.append((i, column_i))
                continue

            # above
            if tree > max(column[0:i]):
                trees_found.append((i, column_i))

    return len(trees_found)


def part2():
    with open('data.txt') as file:
        rows = [line.strip() for line in file.readlines()]

    # remove the edges
    possible_trees = [(tr, tc) for tr, tc in trees_found if
                      tr != 0 and tc != 0 and tr != len(rows[0]) - 1 and tc != len(rows) - 1]

    highest_scenic_score = 0
    for tree_row_i, tree_column_i in possible_trees:
        tree = rows[tree_row_i][tree_column_i]
        row = rows[tree_row_i]
        column = [*zip(*rows)][tree_column_i]

        # ranges with how far you are from the edged
        up, right, down, left = [
            range(1, tree_row_i + 1),
            range(1, len(row) - tree_column_i),
            range(1, len(column) - tree_row_i),
            range(1, tree_column_i + 1),
        ]

        scenic_score = 0
        for x in up:
            scenic_score = scenic_score + 1
            if tree <= rows[tree_row_i - x][tree_column_i]:
                break

        right_trees = 0
        for x in right:
            right_trees = right_trees + 1
            if tree <= rows[tree_row_i][tree_column_i + x]:
                break
        scenic_score = scenic_score * right_trees

        down_trees = 0
        for x in down:
            down_trees = down_trees + 1
            if tree <= rows[tree_row_i + x][tree_column_i]:
                break
        scenic_score = scenic_score * down_trees

        left_trees = 0
        for x in left:
            left_trees = left_trees + 1
            if tree <= rows[tree_row_i][tree_column_i - x]:
                break
        highest_scenic_score = max(highest_scenic_score, scenic_score * left_trees)
    return highest_scenic_score


if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution: {part2()}')
