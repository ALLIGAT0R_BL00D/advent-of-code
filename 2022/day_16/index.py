import re

o, f, v = 'open', 'flow_rate', 'valves'
# minutes = 30


def abc(valves, valve, minutes, released_pressure):
    if minutes == 0:
        print(valve)
        return released_pressure

    all_pressure_releases = []
    for current_valve in valves[valve][v]:
        all_pressure_releases.append(abc(valves, current_valve, minutes - 1, 0))

    return max(all_pressure_releases)

def part1():
    with open('data.txt') as file:
        lines = [line.strip() for line in file.readlines()]

    valves = {}
    for line in lines:
        valve = line[6:8]
        flow = int(re.findall(r'\d+', line)[0])
        lead_to = line.split('; ')[1][22::].strip().split(', ')

        valves[valve] = {
            o: False,
            f: flow,
            v: lead_to
        }
        print(valves[valve])

    all_pressure_releases = []
    for valve in valves.keys():
        all_pressure_releases.append(abc(valves, valve, 30, 0))

    # for minute in range(minutes):
    #     print(f'== Minute {minute + 1} ==')
    #
    #     open_valves = [valve for valve, specs in valves.items() if specs[o]]
    #
    #     if len(open_valves) == 0 and current_valve == start_valve:
    #         print('No valves are open.')
    #
    #         if valves[start_valve][f] == 0:
    #             current_valve = [valve for valve in valves[start_valve][v] if not valves[valve][o]][0]
    #             print(f'You move to valve {current_valve}.')
    #     else:
    #         if not valves[current_valve][o]:
    #             if len(open_valves) == 0:
    #                 print('No valves are open.')
    #             print(f'You open valve {current_valve}.')
    #             valves[current_valve][o] = True
    #         else:
    #             total_pressure = sum(valves[valve][f] for valve in open_valves)
    #             print(f'Valve {", ".join(open_valves)} is open, releasing {total_pressure} pressure.')
    #             pressure_released = pressure_released + total_pressure
    #             closed_valves = [valve for valve in valves[current_valve][v] if not valves[valve][o]]
    #             if len(closed_valves) > 0:
    #                 current_valve = closed_valves[0]
    #             print(f'You open valve {current_valve}.')
    #
    #
    #     print()

    print(all_pressure_releases)
    return max(all_pressure_releases)


def part2():
    with open('data.txt') as file:
        actions = file.readline().split(', ')

    return -1


if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution: {part2()}')
