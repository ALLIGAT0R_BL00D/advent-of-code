def part(decryption_key=1, rounds=1):
    with open('data.txt') as file:
        numbers = [(i, int(x.strip()) * decryption_key) for i, x in enumerate(file.readlines())]

    copy = [*numbers]
    size = len(numbers)
    for _ in range(rounds):
        for i, number in numbers:
            index = copy.index((i, number))
            copy.remove((i, number))

            # step improvement https://youtu.be/QgS36OQxAgE?t=365
            new_index = (index + number) % (size - 1)
            copy.insert(new_index, (i, number))

    copy = [n for _, n in copy]
    index0 = copy.index(0)
    return sum(copy[(x + index0) % size] for x in [1000, 2000, 3000])


if __name__ == '__main__':
    print(f'part1 solution: {part()}')
    print(f'part2 solution: {part(811589153, 10)}')
