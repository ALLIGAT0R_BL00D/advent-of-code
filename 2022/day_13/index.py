def compare(left, right):
    if type(left) == int and type(right) == int:
        if left == right:
            return None
        return left < right

    # convert number to array
    if type(left) == int and type(right) == list:
        left = [left]
    if type(right) == int and type(left) == list:
        right = [right]

    # go thought the least amount of elements
    for i in range(len(left) if len(left) < len(right) else len(right)):
        result = compare(left[i], right[i])
        if result is None:
            continue
        return result

    if len(left) != len(right):
        return len(left) < len(right)
    return None


# Function to find the partition position
def partition(array, low, high):
    # choose the rightmost element as pivot
    pivot = array[high]

    # pointer for greater element
    i = low - 1

    # traverse through all elements
    # compare each element with pivot
    for j in range(low, high):
        if compare(array[j], pivot):
            # If element smaller than pivot is found
            # swap it with the greater element pointed by i
            i = i + 1

            # Swapping element at i with element at j
            (array[i], array[j]) = (array[j], array[i])

    # Swap the pivot element with the greater element specified by i
    (array[i + 1], array[high]) = (array[high], array[i + 1])

    # Return the position from where partition is done
    return i + 1


# function to perform quicksort


def quicksort(array, low, high):
    if low < high:
        # Find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = partition(array, low, high)

        # Recursive call on the left of pivot
        quicksort(array, low, pi - 1)

        # Recursive call on the right of pivot
        quicksort(array, pi + 1, high)


def part1():
    with open('data.txt') as file:
        lines = [line.strip() for line in file.readlines()]

    pairs = {}
    index = 1
    for i, line in enumerate(lines):
        if line == '':
            index = index + 1
            continue

        # todo: remove eval and create function
        if index not in pairs:
            pairs[index] = {'left': eval(line)}
            continue

        pairs[index]['right'] = eval(line)

    indices = []
    for index, (left, right) in pairs.items():
        if compare(pairs[index][left], pairs[index][right]):
            indices.append(index)

    return sum(indices)


def part2():
    with open('data.txt') as file:
        lines = [line.strip() for line in file.readlines()]

    pairs = {
        1: [[2]],
        2: [[6]]
    }
    index = 3
    for i, line in enumerate(lines):
        if line == '':
            continue

        pairs[index] = eval(line)
        index = index + 1

    packages = [*pairs.values()]
    print(packages)
    quicksort(packages, 0, len(packages) - 1)
    print(packages)

    return (packages.index(pairs[1]) + 1) * (packages.index(pairs[2]) + 1)


if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution: {part2()}')
