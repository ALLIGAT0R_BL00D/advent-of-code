lowercase_offset = 96
uppercase_offset = 38


def part1():
    with open('data.txt') as file:
        rucksacks = [l.strip() for l in file.readlines()]

    matches = []
    for rucksack in rucksacks:
        half = len(rucksack) // 2
        matches.append(list(set(rucksack[0:half]).intersection(rucksack[half::]))[0])

    return sum(ord(x) - (lowercase_offset if x.islower() else uppercase_offset) for x in matches)


def part2():
    group_size = 3
    with open('data.txt') as file:
        rucksacks = [l.strip() for l in file.readlines()]

    # groups elves in groups of three
    index = 0
    groups = [[] for _ in range(len(rucksacks) // group_size)]
    for i, rucksack in enumerate(rucksacks):
        if i % group_size == 0 and i != 0:
            index = index + 1

        groups[index].append(rucksack)

    matches = [list(set(sacks[0]) & set(sacks[1]) & set(sacks[2]))[0] for sacks in groups]
    return sum(ord(x) - (lowercase_offset if x.islower() else uppercase_offset) for x in matches)


if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution: {part2()}')
