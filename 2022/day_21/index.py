def part1():
    with open('data.txt') as file:
        actions = file.readline().split(', ')

    return -1


def part2():
    with open('data.txt') as file:
        actions = file.readline().split(', ')

    return -1

if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution: {part2()}')
