def part1():
    with open('data.txt') as file:
        instructions = [line.strip() for line in file.readlines()]

    cycle, x, total_x = 0, 1, 0
    for instruction in instructions:
        cycles = int(instruction.split(' ')[1]) if 'add' in instruction else 0
        for _ in range(1 if "noop" in instruction else 2):
            cycle = cycle + 1

            if cycle % 40 == 20:
                total_x = total_x + cycle * x
        x = x + cycles
    return total_x


def part2():
    with open('data.txt') as file:
        instructions = [line.strip() for line in file.readlines()]

    grid, index = [[]], 0
    cycle = 0
    position = (0, 3)
    for instruction in instructions:
        cycles = int(instruction.split(' ')[1]) if 'add' in instruction else 0
        for _ in range(1 if "noop" in instruction else 2):
            grid[index].append(cycle % 40 in range(*position))
            cycle = cycle + 1

            if cycle % 40 == 0:
                grid.append([])
                index = index + 1
        position = (position[0] + cycles, position[1] + cycles)

    for row in grid:
        print(''.join('#' if _ else ' ' for _ in row))


if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution:\n\033[1;36;40m')
    part2()
