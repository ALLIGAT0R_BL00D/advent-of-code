import re


def relief(worry_level: int) -> int:
    return worry_level // 3


def get_nums_form_line(line: str) -> list:
    return [int(x) for x in re.findall(r'\d+', line)]


def get_num_from_line(line: str) -> int:
    return get_nums_form_line(line)[0]


def get_operation(line: str):
    _, operator, number = line.split(' ')[-3::]

    operations = {
        '+': lambda old: old + int(number),
        '*': lambda old: old * (old if number == 'old' else int(number))
    }

    return operations[operator]


def get_test_number(line):
    return get_num_from_line(line)


def part(rounds: int) -> int:
    with open('data.txt') as file:
        lines = [line.strip() for line in file.readlines()]

    monkey, worry_levels, operation, test, true, false = 'Monkey', 'Starting', 'Operation', 'Test', 'true', 'false'
    monkeys = {}

    # Build all monkey starting data
    current_monkey = None
    for line in lines:
        if monkey in line:
            current_monkey = int(line.split(' ')[1][0:-1])
            monkeys[current_monkey] = {
                worry_levels: [],
                operation: lambda x: x,
                test: 0,
                True: 0,
                False: 0,
            }

        if worry_levels in line:
            monkeys[current_monkey][worry_levels] = get_nums_form_line(line)
        elif operation in line:
            monkeys[current_monkey][operation] = get_operation(line)
        elif test in line:
            monkeys[current_monkey][test] = get_test_number(line)
        elif true in line:
            monkeys[current_monkey][True] = get_num_from_line(line)
        elif false in line:
            monkeys[current_monkey][False] = get_num_from_line(line)

    common_multiple = 1
    for monkey_data in monkeys.values():
        common_multiple = common_multiple * monkey_data[test]

    monkey_business = {monkey_id: 0 for monkey_id in monkeys.keys()}
    for _ in range(rounds):
        for monkey_id, data in monkeys.items():
            monkey_business[monkey_id] = monkey_business[monkey_id] + len(data[worry_levels])

            for worry_level in data[worry_levels]:
                # inspection starts
                worry_level = data[operation](worry_level) % common_multiple

                # monkey gets bored (part 1)
                if rounds == 20:
                    worry_level = relief(worry_level)

                # throw to other monkey based on test
                remains = worry_level % data[test]
                monkeys[data[remains == 0]][worry_levels].append(worry_level)
            data[worry_levels] = []

    sorted_monkey_business = sorted(monkey_business.values())
    return sorted_monkey_business[-2] * sorted_monkey_business[-1]


if __name__ == '__main__':
    print(f'part1 solution: {part(20)}')
    print(f'part2 solution: {part(10_000)}')
