def part1():
    with open('data.txt') as file:
        pairs = [l.strip().split(',') for l in file.readlines()]

    total = 0
    for pair in pairs:
        group1min, group1max = [int(x) for x in pair[0].split('-')]
        group2min, group2max = [int(x) for x in pair[1].split('-')]

        if (group1min >= group2min and group1max <= group2max) or (group2min >= group1min and group2max <= group1max):
            total = total + 1
    return total


def part2():
    with open('data.txt') as file:
        pairs = [l.strip().split(',') for l in file.readlines()]

    total = 0
    for pair in pairs:
        group1min, group1max = [int(x) for x in pair[0].split('-')]
        group2min, group2max = [int(x) for x in pair[1].split('-')]

        if not (group1min > group2max or group2min > group1max):
            total = total + 1
    return total


if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution: {part2()}')
