def part1():
    with open('data.txt') as file:
        rounds = [x.strip() for x in file.readlines()]

    win_points, draw_points = [6, 3]
    rock, paper, sciscors = ['rock', 'paper', 'sciscors']
    map = {'A': rock, 'B': paper, 'C': sciscors, 'X': rock, 'Y': paper, 'Z': sciscors}
    points = {rock: 1, paper: 2, sciscors: 3}

    total_points = 0
    for round in rounds:
        opponent, me = [map[round[0]], map[round[-1]]]
        total_points = total_points + points[me]

        if opponent == me:
            total_points = total_points + draw_points
        elif (opponent == rock and me == paper) or \
            (opponent == paper and me == sciscors) or \
            (opponent == sciscors and me == rock):
            total_points = total_points + win_points

    return total_points


def part2():
    with open('data.txt') as file:
        rounds = [x.strip() for x in file.readlines()]

    win_points, draw_points = [6, 3]
    outcome = {'lose': 'X', 'draw': 'Y', 'win': 'Z'}
    rock, paper, sciscors = ['rock', 'paper', 'sciscors']
    map = {
        'A': rock, 'B': paper, 'C': sciscors,
        outcome['lose']: rock, outcome['draw']: paper, outcome['win']: sciscors
    }
    win = {rock: paper, paper: sciscors, sciscors: rock}
    draw = {rock: rock, paper: paper, sciscors: sciscors}
    lose = {rock: sciscors, paper: rock, sciscors: paper}
    points = {rock: 1, paper: 2, sciscors: 3}

    total_points = 0
    for round in rounds:
        result, opponent = [round[-1], map[round[0]]]

        if result == outcome['win']:
            total_points = total_points + win_points + points[win[opponent]]
        elif result == outcome['draw']:
            total_points = total_points + draw_points + points[draw[opponent]]
        elif result == outcome['lose']:
            total_points = total_points + points[lose[opponent]]

    return total_points


if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution: {part2()}')
