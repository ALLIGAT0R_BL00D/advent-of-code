def part1():
    sequence_length = 4
    with open('data.txt') as file:
        code = file.readline()

    for i in range(len(code)):
        if len(set(code[i:i + sequence_length])) == sequence_length:
            return i + sequence_length
    return -1


def part2():
    sequence_length = 14
    with open('data.txt') as file:
        code = file.readline()

    for i in range(len(code)):
        if len(set(code[i:i + sequence_length])) == sequence_length:
            return i + sequence_length
    return -1


if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution: {part2()}')
