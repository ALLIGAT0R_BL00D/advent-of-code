import os
from time import sleep

r, c = 0, 1
rock, air, sand = '🪨 ', '🍃', '⌛'
leak_x, leak_y = 500, 0


def part1():
    with open('data.txt') as file:
        lines = [line.strip() for line in file.readlines()]

    rock_lines = []

    x_min, x_max, y_max = 999_999_999, 0, 0
    for i, line in enumerate(lines):
        rock_lines.append([])
        coordinates = line.split(' -> ')
        for coordinate in coordinates:
            x, y = [int(n) for n in coordinate.split(',')]
            x_min = min(x_min, x)
            x_max = max(x_max, x)
            y_max = max(y_max, y)
            rock_lines[i].append((y, x))

    # shift x
    x_max = x_max - x_min
    rock_lines = [[(y, x - x_min) for y, x in row] for row in rock_lines]

    # initialize cave
    cave = [[air for _ in range(x_max + 1)] for _ in range(y_max + 1)]

    # make cave
    for row in rock_lines:
        for i in range(1, len(row)):
            from_y, from_x, to_y, to_x = *row[i - 1], *row[i]

            # draw rocks in the x-axis
            x_direction = 1 if from_x < to_x else -1
            for x in range(from_x, to_x + x_direction, x_direction):
                cave[from_y][x] = rock

            # draw rocks in the y-axis
            y_direction = 1 if from_y < to_y else -1
            for y in range(from_y, to_y + y_direction, y_direction):
                cave[y][from_x] = rock

    leak = [500 - x_min, 0]

    # keep dropping sand
    number_of_sand = 0
    stop = False
    while not stop:
        sand_position = (leak[c], leak[r])
        while True:
            down = (sand_position[r] + 1, sand_position[c])
            down_left = (sand_position[r] + 1, sand_position[c] - 1)
            down_right = (sand_position[r] + 1, sand_position[c] + 1)

            if cave[down[r]][down[c]] == air:
                sand_position = down
            elif cave[down_left[r]][down_left[c]] == air:
                sand_position = down_left
            elif cave[down_right[r]][down_right[c]] == air:
                sand_position = down_right
            else:
                cave[sand_position[r]][sand_position[c]] = sand
                number_of_sand = number_of_sand + 1
                break

            # outside cave
            if down[r] == y_max or down_left[c] < 0 or down_right[c] > x_max:
                stop = True
                break

            # visualize
            # for rr, row in enumerate(cave):
            #     print(''.join([value if (rr, cc) != sand_position else sand for cc, value in enumerate(row)]))
            # sleep(.1)
            # os.system('clear')

    for rr, row in enumerate(cave):
        print(''.join(row))
    return number_of_sand


def part2():
    with open('data.txt') as file:
        lines = [line.strip() for line in file.readlines()]

    rock_lines = []

    x_min, x_max, y_max = 999_999_999, 0, 0
    for i, line in enumerate(lines):
        rock_lines.append([])
        coordinates = line.split(' -> ')
        for coordinate in coordinates:
            x, y = [int(n) for n in coordinate.split(',')]
            x_min = min(x_min, x)
            x_max = max(x_max, x)
            y_max = max(y_max, y)
            rock_lines[i].append((y, x))

    y_max = y_max + 2  # new floor
    padding_left, padding_right = y_max - (leak_x - x_min), y_max - (x_max - leak_x)

    # shift x
    x_max = x_max - x_min + padding_left + padding_right
    rock_lines = [[(y, x - x_min + padding_left) for y, x in row] for row in rock_lines]

    # initialize cave
    cave = [[air for _ in range(x_max + 1)] for _ in range(y_max + 1)]

    # make cave
    for row in rock_lines:
        for i in range(1, len(row)):
            from_y, from_x, to_y, to_x = *row[i - 1], *row[i]

            # draw rocks in the x-axis
            x_direction = 1 if from_x < to_x else -1
            for x in range(from_x, to_x + x_direction, x_direction):
                cave[from_y][x] = rock

            # draw rocks in the y-axis
            y_direction = 1 if from_y < to_y else -1
            for y in range(from_y, to_y + y_direction, y_direction):
                cave[y][from_x] = rock

    leak_position = [leak_y, leak_x - x_min + padding_left]

    # make floor
    cave[len(cave) - 1] = [rock for _ in cave[0]]

    # keep dropping sand
    number_of_sand = 0
    stop = False
    while not stop:
        sand_position = leak_position
        while True:
            down = (sand_position[r] + 1, sand_position[c])
            down_left = (sand_position[r] + 1, sand_position[c] - 1)
            down_right = (sand_position[r] + 1, sand_position[c] + 1)

            if cave[down[r]][down[c]] == air:
                sand_position = down
            elif cave[down_left[r]][down_left[c]] == air:
                sand_position = down_left
            elif cave[down_right[r]][down_right[c]] == air:
                sand_position = down_right
            else:
                cave[sand_position[r]][sand_position[c]] = sand
                number_of_sand = number_of_sand + 1
                if sand_position == leak_position:
                    stop = True
                break

            # visualize
            for rr, row in enumerate(cave):
                print(''.join([value if (rr, cc) != sand_position else sand for cc, value in enumerate(row)]))
            sleep(.1)
            os.system('clear')
    return number_of_sand


if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution: {part2()}')
