import re

x, y = 0, 1


def part1():
    with open('data.txt') as file:
        lines = [line.strip() for line in file.readlines()]

    pairs = []
    for i, line in enumerate(lines):
        sensor_x, sensor_y, beacon_x, beacon_y = [int(n) for n in re.findall(r'-?\d+', line)]
        pairs.append(((sensor_x, sensor_y), (beacon_x, beacon_y)))

    no_beacon = set()
    for sensor_, beacon_ in pairs:
        distance = abs(sensor_[x] - beacon_[x]) + abs(sensor_[y] - beacon_[y])
        range_y = range(sensor_[y] - distance, sensor_[y] + distance + 1)

        if row not in range_y:
            continue

        distance_to_row = abs(sensor_[y] - row)
        for x_ in range(sensor_[x] - distance + distance_to_row, sensor_[x] + distance - distance_to_row + 1):
            if (x_, row) != sensor_ and (x_, row) != beacon_:
                no_beacon.add(x_)

    return len(no_beacon)


def part2():
    with open('data.txt') as file:
        lines = [line.strip() for line in file.readlines()]

    pairs = []
    for i, line in enumerate(lines):
        sensor_x, sensor_y, beacon_x, beacon_y = [int(n) for n in re.findall(r'-?\d+', line)]
        pairs.append(((sensor_x, sensor_y), (beacon_x, beacon_y)))

    # explanation
    # https://www.youtube.com/watch?v=w7m48_uCvWI&t=834s&ab_channel=WilliamY.Feng
    positive_lines = []
    negative_lines = []
    for sensor_, beacon_ in pairs:
        distance = abs(sensor_[x] - beacon_[x]) + abs(sensor_[y] - beacon_[y])
        positive_lines.extend([sensor_[x] - sensor_[y] - distance, sensor_[x] - sensor_[y] + distance])
        negative_lines.extend([sensor_[x] + sensor_[y] - distance, sensor_[x] + sensor_[y] + distance])

    positive = None
    negative = None
    # compare all parallel lines
    for i in range(len(pairs) * 2):
        for j in range(i + 1, len(pairs) * 2):
            # parallel positive lines
            line1, line2 = positive_lines[i], positive_lines[j]

            # gap of two between lines will leave a spot fot the distress beacon
            if abs(line1-line2) == 2:
                positive = min(line1, line2) + 1

            # parallel positive lines
            line1, line2 = negative_lines[i], negative_lines[j]

            # gap of two between lines will leave a spot fot the distress beacon
            if abs(line1 - line2) == 2:
                negative = min(line1, line2) + 1

    distress_beacon = ((positive + negative) // 2, (negative-positive) // 2)

    return distress_beacon[x] * 4_000_000 + distress_beacon[y]


if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution: {part2()}')
