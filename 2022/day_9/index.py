from helpers.time import run


def outside_range(tail, head):
    check = set()
    for x in range(tail[0] - 1, tail[0] + 2):
        for y in range(tail[1] - 1, tail[1] + 2):
            check.add((x, y))
    return head not in check


def part1():
    with open('data.txt') as file:
        instructions = [[line[0], int(line[2::])] for line in file.readlines()]

    previous_head, head, tail = (0, 0), (0, 0), (0, 0)
    visited = {tail}
    new_head = {
        'U': lambda old: (old[0], old[1] + 1),
        'R': lambda old: (old[0] + 1, old[1]),
        'D': lambda old: (old[0], old[1] - 1),
        'L': lambda old: (old[0] - 1, old[1]),
    }

    for instruction, steps in instructions:
        for x in range(1, steps + 1):
            if outside_range(tail, head):
                tail = previous_head

            previous_head = head
            head = new_head[instruction](head)
            visited.add(tail)

    return len(visited)


def part2():
    with open('data.txt') as file:
        actions = file.readline().split(', ')

    return -1


if __name__ == '__main__':
    print(f'part1 solution: {run(part1)}')
    print(f'part2 solution: {run(part2)}')
