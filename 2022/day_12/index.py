from typing import List, Tuple, Dict

Coordinate = Tuple[int, int]
Coordinates = List[Coordinate]
Grid = List[List[Tuple[int, int, int]]]
Trace = Dict[Coordinate, Coordinate]
Cost = Dict[Coordinate, int]
r, c, h = range(3)
too_high = 999_999_999


def cost_to_destination(position: Coordinate, goal: Coordinate):
    """
    Also known as h(n).
    Calculates the cost to the destination to steer the algorithm in the right direction
    """
    return abs(position[0] - goal[0]) + abs(position[1] - goal[1])


def cost_to_move(heightmap: Grid, position: Coordinate, neighbor: Coordinate):
    """
    Also known as d(current, neighbor)
    Calculate the cost to move to the neighbor.
    You can only move to a height that is one higher than your position.
    Heights from your position that are higher than one are counted as walls
    """
    height_difference = heightmap[neighbor[r]][neighbor[c]][h] - heightmap[position[r]][position[c]][h]
    return 1 if height_difference <= 1 else too_high


def reconstruct_route(traceback: Trace, position: Coordinate):
    """
    Trace back the steps we took to get to the destination
    """
    path = []
    while position in traceback.keys():
        position = traceback[position]
        path.append(position)
    return path


def part(all_starts: bool = False):
    with open('data.txt') as file:
        lines = [line.strip() for line in file.readlines()]

    starting_points: List[Coordinate] = []
    destination: Coordinate = (0, 0)
    heightmap: Grid = []
    step_cost: Cost = {}

    # build heightmap grid
    for row, line in enumerate(lines):
        heightmap.append([])
        for column, height in enumerate(line):
            step_cost[(row, column)] = too_high
            if height == 'S' or (all_starts and height == 'a'):
                starting_points.append((row, column))
                heightmap[row].append((row, column, ord('a')))
            elif height == 'E':
                destination = (row, column)
                heightmap[row].append((row, column, ord('z')))
            else:
                heightmap[row].append((row, column, ord(height)))

    shortest_path = too_high
    for start in starting_points:
        step_cost[start] = 0
        discovered: Coordinates = [start]
        traceback: Trace = {}

        while len(discovered) != 0:
            # get the discovered coordinate with the lowest destination cost
            current_position: Coordinate = min(discovered, key=lambda x: step_cost[x])

            if current_position == destination:
                shortest_path = min(shortest_path, len(reconstruct_route(traceback, current_position)))

            # remove the current position from discovered
            discovered = [x for x in discovered if x != current_position]

            # define neighbors of the current position
            neighbors: Coordinates = [
                (current_position[r] - 1, current_position[c]),  # top
                (current_position[r], current_position[c] + 1),  # right
                (current_position[r] + 1, current_position[c]),  # bottom
                (current_position[r], current_position[c] - 1),  # left
            ]

            # ignore out of bounds and other starting points (either closer or going back)
            neighbors = [
                (nr, nc) for nr, nc in neighbors
                if 0 <= nr < len(heightmap) and 0 <= nc < len(heightmap[nr]) and (nr, nc) not in starting_points
            ]

            # 'discover' the neighbors and calculate their costs
            for neighbor in neighbors:
                move_cost: int = step_cost[current_position] + cost_to_move(heightmap, current_position, neighbor)

                # this neighbor is a good option to move to
                if move_cost < step_cost[neighbor]:
                    traceback[neighbor] = current_position
                    step_cost[neighbor] = move_cost

                    if neighbor not in discovered:
                        discovered.append(neighbor)
    return shortest_path


if __name__ == '__main__':
    print(f'part1 solution: {part()}')
    print(f'part2 solution: {part(True)}')
