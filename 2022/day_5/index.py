import re

# just a lazy way to make the split between stack and instructions 🙂
stack_indexes_line = 8
instruction_start = stack_indexes_line + 2


def part1():
    with open('data.txt') as file:
        rows = file.readlines()

    stacks_indexes = [(int(x[0]), int(x.start())) for x in re.finditer(r'\d', rows[stack_indexes_line])]
    stacks_raw = rows[0:stack_indexes_line]
    instructions = rows[instruction_start::]

    stacks = {s: [] for s, _ in stacks_indexes}
    for stack_row in stacks_raw:
        for stack_number, stack_index in stacks_indexes:
            if stack_index > len(stack_row) or stack_row[stack_index] == ' ':
                continue
            stacks[stack_number].append(stack_row[stack_index])

    for instruction in instructions:
        move, from_, to = [int(x) for x in re.findall(r'\d+', instruction)]

        # revert moving array, because the stacks are inverted in the code
        stacks[to] = [*stacks[from_][0:move][::-1], *stacks[to]]
        stacks[from_] = stacks[from_][move::]

    return ''.join([stack[0] for stack in stacks.values()])


def part2():
    with open('data.txt') as file:
        rows = file.readlines()

    stacks_indexes = [(int(x[0]), int(x.start())) for x in re.finditer(r'\d', rows[stack_indexes_line])]
    stacks_raw = rows[0:stack_indexes_line]
    instructions = rows[instruction_start::]

    stacks = {s: [] for s, _ in stacks_indexes}
    for stack_row in stacks_raw:
        for stack_number, stack_index in stacks_indexes:
            if stack_index > len(stack_row) or stack_row[stack_index] == ' ':
                continue
            stacks[stack_number].append(stack_row[stack_index])

    for instruction in instructions:
        move, from_, to = [int(x) for x in re.findall(r'\d+', instruction)]

        stacks[to] = [*stacks[from_][0:move][::], *stacks[to]]
        stacks[from_] = stacks[from_][move::]

    return ''.join([stack[0] for stack in stacks.values()])


if __name__ == '__main__':
    print(f'part1 solution: {part1()}')
    print(f'part2 solution: {part2()}')
